import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

def quadra(k):
    if(k%2==0):
        return 0
    else:
        return 2/(k*np.pi)

def triangolare(k):
    if(k%2==0):
        return 0
    else:
        return (2/(k*np.pi))**2

def wk(k, f):
    return k*f*(2*np.pi)

time_size = 5000
f = 1
tout = np.linspace(-1, 1, time_size) 

list = [1, 3, 5, 9, 49, 99, 499, 999, 4999, 9999]
c = 0
for max_four in list:
    out = np.zeros(time_size)
    for k in range(1, max_four+1):
        out += quadra(k)*np.sin(wk(k, f)*tout)

    plt.figure()
    plt.title("n = " + str(max_four))
    plt.plot(tout, out)
    plt.savefig("./plot_quadra/"+str(c)+".pdf")
    plt.close()
    c = c+1

list = [1, 3, 5, 9, 49, 99]
c = 0
for max_four in list:
    out = np.zeros(time_size)
    for k in range(1, max_four+1):
        out += triangolare(k)*np.cos(wk(k, f)*tout)

    plt.figure()
    plt.title("n = " + str(max_four))
    plt.plot(tout, out)
    plt.savefig("./plot_triangolare/"+str(c)+".pdf")
    plt.close()
    c = c+1


