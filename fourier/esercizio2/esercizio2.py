import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

#Restituisce i coefficienti dei seni per avere un'onda quadra
def quadra(n):
    if n%2==0:
        return 0
    else:
        return 2/(n*np.pi)

for case in range(1,11):
    #I dati da prendere
    t, x, y = np.loadtxt('../dati/DSO000'+str(case).rjust(2, '0')+'.CSV', delimiter=',', skiprows=2, usecols=(0,1,2), unpack=True)

    #Cerco il periodo dell'onda quadra guardando quando cambia segno
    #ts e ns segnano i tempi e i numeri dei dati in cui c'e' il cambio segno
    tmp=np.sign(x[0])
    ts=np.zeros(0)
    ns=np.zeros(0)

    for i in range(len(t)):
        if tmp==-1 and x[i]>0:
            ts=np.append(ts,t[i])
            ns=np.append(ns,i)
            tmp=1
        elif tmp==1 and x[i]<0:
            ts=np.append(ts,t[i])
            ns=np.append(ns,i)
            tmp=-1
        elif tmp==0:
            tmp=np.sign(x[i])

    #Faccio le differenze tra i ts e ns per avere la media
    #del periodo in secondi e in numero di dati
    delta=np.zeros(len(ts)-1)
    deltan=np.zeros(len(ts)-1)
    for i in range(len(ts)-1):
        delta[i]=ts[i+1]-ts[i]
        deltan[i]=ns[i+1]-ns[i]

    #la media e' il semiperiodo
    T, dT = 2*np.mean(delta), 2*np.std(delta)

    #Faccio una media dei vari periodi e la sistemo in maniera da centrarla
    dt = t[1]-t[0]
    N = int(np.mean(deltan))
    m = np.zeros(2*N)
    n = np.zeros(2*N)
    tmp = np.zeros(2*N)

    for i in range(len(t)):
        j = int(i%(2*N)-ns[0])
        if j<0:
            j+= 2*N
        m[j]+=y[i]
        n[j]+=x[i]
        tmp[j]+=1

    for i in range(2*N):
        m[i]/=tmp[i]
        n[i]/=tmp[i]

    #Trovo la Vpp come massimo meno minimo
    Vpp=np.max(n)-np.min(n)


    f=1/T
    #Frequenza di taglio nominale
    ft0=219
    w=np.zeros(2*N)

    #La funzione di best-fit, e' la serie di fourier troncata
    #a 300 termini e valutata nel punto j, sapendo che
    #l'onda quadra ha periodo 2*N
    def func(j, ft, offset):
        a=0
        for i in range(300):
            a += 1/(1+(i*f/ft)**2)**0.5 * quadra(i) * np.sin(2*np.pi*i*j/(2*N) + np.arctan(-i*f/ft))
        return a*Vpp*np.sign(x[int(ns[0])+1])+offset

    #Effettua il fit
    f0, df0 = curve_fit(func, np.array(range(len(m))), m, [ft0,0])

    print(f0)
    print(df0)
    print(str(f0[0])+'+/-'+str(df0[0][0]**0.5))

    #Disegna la funzione di best-fit
    for j in range(2*N):
        w[j]+=func(j, f0[0], 0)
    w = w+f0[1]

    plt.figure(str(case))
    plt.plot(range(2*N),w)
    plt.plot(range(2*N),m)
    plt.savefig('./images/es2_'+str(case)+'.pdf',format='pdf')

plt.show()

