import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

#i coefficienti dell'onda quadra nella base dei seni
def quadra(k):
    if(k%2==0):
        return 0
    else:
        return 2/(k*np.pi)

#fattore di attenuazione del guadagno
def A(f, fT):
    return 1/np.sqrt(1+(f/fT)**2)

#sfasamento
def sf(f, fT):
    return np.arctan(-f/fT)

#pulsazone \omega_k
def wk(k, f):
    return k*f*(2*np.pi)

for case in range(1,11):
    #I dati da prendere
    t, x, y = np.loadtxt('../dati/DSO000'+str(case).rjust(2, '0')+'.CSV', delimiter=',', skiprows=2, usecols=(0,1,2), unpack=True)

    #qualche riga per trovare il periodo e trovarmi il primo salto verso l'alto, prendo anche l'ampiezza picco picco
    last = 0
    #registra serve per saltare il primo periodo che mi riduce la media
    registra = False
    first = -1 
    T_array = []
    for i in range(len(t)):
        if(np.sign(x[i]) != np.sign(x[last])):
            if(registra):
                T_array.append(t[i]-t[last])
            if(x[last] < 0 and x[i] > 0 and first == -1):
                first = i
            registra = True
            last = i

    #nel caso 10 ci sono due punti che danno fastidio... ignoro quei due punti 
    if(case == 10):
        del T_array[3]

    T_array = np.array(T_array)
    T = 2*T_array.mean()
    Vpp = abs(x).mean()

    print("Vpp: " + str(Vpp))
    print("T: " + str(T))

    #shifto in modo tale che a t = 0 si misura la prima salita
    t = t-t[first]

    f = 1/T 
    fT = 215 

    time_size = 5000
    tout = np.linspace(t[0], t[len(t)-1], time_size) 
    out = np.zeros(time_size)

    max_four = 200
    for k in range(1, max_four, 1):
        out += 2*Vpp*A(k*f, fT)*quadra(k)*np.sin(wk(k, f)*tout+sf(k*f, fT))

    plt.figure(str(case))
    plt.plot(tout, out)
    plt.plot(t, y)
    plt.ylabel('V [V]')
    plt.xlabel('t [s]')
    #diminuisco un po' il font sull'asse x
    plt.rc('xtick', labelsize=6)
    plt.savefig("./esercizio2_modificato_plot/"+str(case)+".pdf")


