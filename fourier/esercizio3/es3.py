import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

#Solo seni
def quadra(n):
    if n%2==0:
        return 0
    else:
        return 2/(n*np.pi)

f=np.array([])
errf=np.array([])
a=np.array([])
erra=np.array([])
a1= np.array([])
#Il valore della frequenza di taglio nominale
ft0=219

for case in range(1,11):
    t, x, y = np.loadtxt('../dati/DSO000'+str(0 if case<10 else '')+str(case)+'.CSV', delimiter=',',skiprows=2,usecols=(0,1,2),   unpack=True)


    #Cerco il periodo dell'onda quadra
    tmp=np.sign(x[0])
    ts=np.zeros(0)
    ns=np.zeros(0)

    for i in range(len(t)):
        if tmp==-1 and x[i]>0:
            ts=np.append(ts,t[i])
            ns=np.append(ns,i)
            tmp=1
        elif tmp==1 and x[i]<0:
            ts=np.append(ts,t[i])
            ns=np.append(ns,i)
            tmp=-1
        elif tmp==0:
            tmp=np.sign(x[i])

    delta=np.zeros(len(ts)-1)
    deltan=np.zeros(len(ts)-1)
    for i in range(len(ts)-1):
        delta[i]=ts[i+1]-ts[i]
        deltan[i]=ns[i+1]-ns[i]

    T, dT = 2*np.mean(delta), 2*np.std(delta)
    f = np.append(f, 1/T)
    errf = np.append(errf, dT/T**2)
    #Faccio una media dei vari periodi
    dt = t[1]-t[0]
    N = int(np.mean(deltan))
    '''
    m = np.zeros(2*N)
    n = np.zeros(2*N)
    tmp = np.zeros(2*N)

    for i in range(len(t)):
        j = int(i%(2*N)-ns[0])
        if j<0:
            j+= 2*N
        m[j]+=y[i]
        n[j]+=x[i]
        tmp[j]+=1

    for i in range(2*N):
        m[i]/=tmp[i]
        n[i]/=tmp[i]
    '''
    #Trovo i valori picco picco di entrambi i dataset
    Vpp = np.max(x)-np.min(x)
    errVpp = 0.2
    print('Vpp:',Vpp)
    Vft = [15.1,14.8,13.6,11.2,7.2,3.6,2.5,1.3,0.5,0.3]
    errVf = [0.2,0.2,0.2,0.2,0.2,0.2,0.1,0.1,0.1,0.1]
    Vf=Vft[case-1]
    print('Vf:',Vf)
    a = np.append(a,Vf/Vpp)
    erra = np.append(erra, Vf/Vpp*((errVf[case-1]/Vf)**2+(errVpp/Vpp)**2+2*(0.03)**2)**0.5)

    #plt.figure(str(case))
    #plt.plot(range(2*N),n)
    #plt.plot(range(2*N),m)

tmp=np.logspace(np.log10(np.min(f)),np.log10(np.max(f)),100)
for k in range(100):
    f1=tmp[k]
    w=np.zeros(20)
    for i in range(200):
        for j in range(20):
            w[j]+=1/(1+(i*f1/ft0)**2)**0.5*quadra(i)*np.sin(2*np.pi*i*j/(20)+np.arctan(-i*f1/ft0))
    #plt.plot(range(0,int(2*N)-10,10),Vpp*w*np.sign(n[3]))
    a1 = np.append(a1,(np.max(w)-np.min(w)))

plt.xscale('log')
plt.xlabel('Frequenza (Hz)')
plt.yscale('log')
plt.ylabel('Guadagno')
plt.errorbar(f,a,erra,errf, fmt='.')
plt.plot(tmp,a1)
for i in range(len(a)):
    print(f[i],'\\pm',errf[i],' & ', a[i],'\\pm',erra[i],'\\\\')
plt.savefig('./images/es3.pdf',format='pdf')
plt.show()
