import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

#Solo seni
def quadra(n):
    if n%2==0:
        return 0
    else:
        return 2/(n*np.pi)

#Solo coseni
def triang(n):
    if n%2==0:
        return 0
    else:
        return (2/(n*np.pi))**2

f=np.array([])
errf=np.array([])
erra0=np.array([])
erra1=np.array([])
a0=np.array([])
a1= np.array([])
a2=np.array([])
a3= np.array([])
ft0=49.8
ft1=2340

for case in range(11,19):
    t, x, y, z = np.loadtxt('../dati/DSO000'+str(case)+'.CSV', delimiter=',',skiprows=2,usecols=(0,1,2,3),   unpack=True)


    #Cerco il periodo dell'onda quadra
    tmp=np.sign(x[0])
    ts=np.zeros(0)
    ns=np.zeros(0)

    for i in range(len(t)):
        if tmp==-1 and x[i]>0:
            ts=np.append(ts,t[i])
            ns=np.append(ns,i)
            tmp=1
        elif tmp==1 and x[i]<0:
            ts=np.append(ts,t[i])
            ns=np.append(ns,i)
            tmp=-1
        elif tmp==0:
            tmp=np.sign(x[i])

    delta=np.zeros(len(ts)-1)
    deltan=np.zeros(len(ts)-1)
    for i in range(len(ts)-1):
        delta[i]=ts[i+1]-ts[i]
        deltan[i]=ns[i+1]-ns[i]

    T, dT = 2*np.mean(delta), 2*np.std(delta)
    f = np.append(f, 1/T)
    errf = np.append(errf, dT/T**2)

    #Faccio una media dei vari periodi
    dt = t[1]-t[0]
    N = int(np.mean(deltan))

    #Trovo i valori picco picco di entrambi i dataset
    Vft = [14.8,14.5,12.1,6.6,2.2,1.2,0.7,0.5,0.2]
    Vfft = [0.6,0.6,0.5,0.4,0.4,0.4,0.4,0.4,0.2]
    Vppt = [14.8,14.8,14.8,14.2,14.0,13.8,13.7,13.6,13.7]
    errVpp = 0.2
    errVf = [0.2,0.2,0.1,0.1,0.1,0.1,0.1,0.1,0.1]
    errVff = 0.1
    Vf=Vft[case-11]
    Vff=Vfft[case-11]
    Vpp=Vppt[case-11]
    a0 = np.append(a0,Vf/Vpp)
    erra0 = np.append(erra0, Vf/Vpp*((errVf[case-11]/Vft[case-11])**2+(errVpp/Vppt[case-11])**2+2*(0.03)**2)**0.5)
    a1 = np.append(a1,Vff/Vpp)
    erra1 = np.append(erra1, Vff/Vpp*((errVff/Vfft[case-11])**2+(errVpp/Vppt[case-11])**2)**0.5+2*(0.03)**2)
    '''
    plt.figure(str(case))
    #Ingresso
    plt.plot(range(len(x)),x)
    #Integrato
    plt.plot(range(len(y)),y)
    #Derivato
    plt.plot(range(len(z)),z)
    '''
tmp = np.logspace(np.log10(np.min(f)),np.log10(np.max(f)),100)

for f1 in tmp:
    w1=np.zeros(30)
    for i in range(300):
        for j in range(30):
            w1[j]+=1/(1+(i*f1/ft0)**2)**0.5*quadra(i)*np.sin(2*np.pi*i*j/(30)+np.arctan(-i*f1/ft0))

    w2=np.zeros(30)
    for i in range(1,300):
        for j in range(30):
            w2[j]+=1/(1+(ft1/f1/i)**2)**0.5*1/(1+(i*f1/ft0)**2)**0.5*quadra(i)*np.sin(2*np.pi*i*j/(30)+np.arctan(ft1/f1/i)+np.arctan(-i*f1/ft0))
    #Integrato
    #plt.plot(range(2*N),Vpp*w1)
    #Derivato
    #plt.plot(range(2*N),Vpp*w2)
    a2 = np.append(a2,np.max(w1)-np.min(w1))
    a3 = np.append(a3,np.max(w2)-np.min(w2))

#plt.figure("Finale")
plt.xscale('log')
plt.xlabel('Frequenza [Hz]')
plt.yscale('log')
plt.ylabel('Guadagno')
plt.errorbar(f,a0,erra0,errf,fmt='.')
plt.errorbar(f,a1,erra1,errf,fmt='.')
plt.plot(tmp,a2)
plt.plot(tmp,a3)

for i in range(len(a0)):
    print('$',f[i],'\\pm',errf[i],'$ & $', a0[i],'\\pm',erra0[i],'$ & $', a1[i],'\\pm',erra1[i],'$\\\\')

plt.savefig('./images/es4.1.pdf',format='pdf')
plt.show()
