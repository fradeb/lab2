import numpy as np
import matplotlib.pyplot as plt

def quadra(k):
    if(k%2==0):
        return 0
    else:
        return 2/(k*np.pi)

def A(f, fT):
    return 1/np.sqrt(1+(fT/f)**2)

def sf(f, fT):
    return np.arctan(fT/f)

def wk(k, f):
    return k*f*(2*np.pi)

fl = [40,1000]

for case in range(2):
    f = fl[case]
    fT = 10

    time_size = 5000
    periodi = 2
    t = np.linspace(periodi*(-1/f), periodi*(1/f), time_size)
    out = np.zeros(time_size)

    max_four = 50000
    for k in range(1, max_four, 1):
        out += A(k*f, fT)*quadra(k)*np.sin(wk(k, f)*t+sf(f*k, fT))

    plt.figure("Derivatore "+str(case+1))
    plt.plot(t, out)
    plt.xlabel("Tempo [s]")
    plt.ylabel("Potenziale [V]")
    plt.savefig("derivatore"+str(case)+".pdf")
plt.show()

