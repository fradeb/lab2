import numpy as np
import matplotlib.pyplot as plt

t, x, y, z = np.loadtxt('scope_201209/DSO00025.CSV', delimiter=',',skiprows=2,usecols=(0,1,2,3), unpack=True)

plt.plot(t,x)
plt.plot(t,y)
plt.plot(t,z)
plt.show()
