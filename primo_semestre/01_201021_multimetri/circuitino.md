MISURE POTENZIALE BATTERIA A CIRCUITO APERTO
digitale 5.04V
analogico 25 tacchette (fs 10V) 

MISURE DI RESISTENZA
33.2 Ohm
328 Ohm
6.75 kOhm
328 kOhm
7.04 MOhm

MISURE PRIMO CIRCUITINO
digitale    analogico
R_1
fs 200mA    fs 10V
135.5 mA       22 tacchette

R_2
fs 20mA     fs 10V
14.93mA     24.25 tacchette

R_3
fs 2mA      fs 10V
765 miA     25 tacchette

R_4
fs 200miA   fs 10V
40.5miA     25.25 tacchette

R_5
fs 200miA   fs 10V
26.0miA     25.25 tacchette

inverto i ruoli: analogico = amperometro, digitale = voltmetro

analogico       digitale
R_1
fs 500mA        fs 20V 
13.5 tacche     4.45V

R_2
fs 50mA         fs 20V
14.75 tacche    4.85V

R_3
fs 5mA
7.25 tacche     4.98V

R_4
fs 50miA
15.5 tacche     5.00V

R_5
fs 50miA
1 tacca         5.04V
