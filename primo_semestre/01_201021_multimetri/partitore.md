R_1 nominale 6.8 Mohm
R_2 6.8 kohm

Prendo il voltmetro digitale, primo misuro V1, poi V2 e infine V_tot (una misura alla volta)

V_1             V_2             V_tot
5.03V           4.8mV           5.04V

Ora analogico
V_1             V_2             V_tot
fs 10V          fs 100mV        fs 10V
24.5 tacche     0.5 tacche      25.25 tacche

Riprendo il multimetro digitale e misuro V_tot
V_tot
5.04V
