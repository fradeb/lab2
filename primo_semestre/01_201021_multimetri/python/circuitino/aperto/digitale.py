#laboratorio
import pylab
import numpy as np

dig, perc, last, mult = pylab.loadtxt('digitale_input.txt', unpack = True)

sigma_dig = np.sqrt((last*mult)**2 + (dig*perc/100)**2)

f = open("digitale_output.txt", "w")
f.write("#value\t\t\t#sigma\n")
for i in range(0, len(dig)):
    f.write("{:.5e}".format(dig[i]) + "\t\t" + "{:.5e}".format(sigma_dig[i]) + "\n")
f.close()
