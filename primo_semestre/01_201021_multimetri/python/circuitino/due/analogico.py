#laboratorio
import pylab
import numpy as np

tacche, sigma, fs, perc = pylab.loadtxt('analogico_input.txt', unpack = True)

tot = 50
value = tacche/tot*fs
sigma_value = np.sqrt((sigma/tot*fs)**2 + (perc*fs/100)**2)

f = open("analogico_output.txt", "w")
f.write("#value\t\t\t#sigma\n")
for i in range(0, len(value)):
    f.write("{:.5e}".format(value[i]) + "\t\t" + "{:.5e}".format(sigma_value[i]) + "\n")
f.close()
