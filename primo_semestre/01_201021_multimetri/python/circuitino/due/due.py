#laboratorio
import pylab
import numpy as np

#incertezze rapide
from uncertainties import ufloat
from uncertainties.umath import *

in_a, sigma_a = pylab.loadtxt('a.txt', unpack = True)
in_r, sigma_r = pylab.loadtxt('r.txt', unpack = True)

a = [None]*len(in_a)
r = [None]*len(in_r)

for i in range(0, len(in_a)):
    a[i] = ufloat(in_a[i], sigma_a[i])

for i in range(0, len(in_r)):
    r[i] = ufloat(in_r[i], sigma_r[i])

f = open("due.txt", "w")
for i in range(0, len(a)):
    f.write(str(a[i]*r[i])+"\n")
f.close()



