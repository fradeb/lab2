import pylab
import numpy
from scipy.optimize import curve_fit

# data load
x, y = pylab.loadtxt("datifit/scarica.txt", unpack=True)
Dx = numpy.array([4.]*len(x))
Dy0 = numpy.array([1.]*len(y))
Dy = Dy0

#iterate
for i in range(0,3):
    # use subplots to display two plots in one figure
    # note the syntax
    pylab.rc('font',size=18)
    pylab.subplot(2,1,2)

    # scatter plot with error bars
    pylab.errorbar(x,y,Dy,Dx,linestyle = '', color = 'black', marker = '.')

    # bellurie 
    pylab.ylabel('$\Delta V$  [arb.un.]')
    pylab.xlabel('$t$  [µs]')
    pylab.minorticks_on()

    xmax = 20000
    pylab.xlim(min(x), xmax)

    pylab.ylim(10, max(y))
    pylab.yscale('log')

    # AT THE FIRST ATTEMPT COMMENT FROM HERE TO THE END

    # define the function (linear, in this example)
    def ff(x, A, t, B):
        return A*numpy.exp(-x/t) + B

    # define the initial values (STRICTLY NEEDED!!!)
    init=(1020, 1386, 0)

    # prepare a dummy xx array (with 2000 linearly spaced points)
    xx=numpy.linspace(min(x),max(x),2000)

    # plot the fitting curve computed with initial values
    # AT THE SECOND ATTEMPT THE FOLLOWING LINE MUST BE COMMENTED 
    #pylab.plot(xx,ff(xx,*init), color='blue') 

    # set the error
    sigma=Dy
    w=1/sigma**2

    # call the minimization routine
    pars,covm=curve_fit(ff,x,y,init,sigma, absolute_sigma=True)
    ncovm = numpy.empty([len(pars), len(pars)])
    for i in range(0, len(pars)):
        for j in range(0, len(pars)):
            ncovm[i][j] = covm[i][j]/numpy.sqrt(covm[i][i]*covm[j][j])

    print('normalized: ', ncovm)


    # calculate the chisquare for the best-fit function
    chi2 = ((w*(y-ff(x,*pars))**2)).sum()
    print(ff(0, *pars))

    # determine the ndof
    ndof=len(x)-len(init)

    # print results on the console
    print('pars:',pars)
    print('covm:',covm)
    print ('chi2, ndof:',chi2, ndof)

    # plot the best fit curve
    pylab.plot(xx,ff(xx,*pars), color='orange') 

    # switch to the residual plot
    pylab.subplot(2,1,1)

    # build the array of the normalized residuals
    r = (y-ff(x,*pars))/sigma

    # bellurie 
    pylab.rc('font',size=18)
    pylab.ylabel('Norm. res.')
    pylab.xlim(min(x), xmax)
    pylab.minorticks_on()
    # set the vertical range for the norm res
    pylab.ylim((-6,6))

    # plot residuals as a scatter plot with connecting dashed lines
    pylab.plot(x,r,linestyle="--",color='blue',marker='o')

    for i in range(0, len(Dy)):
        Dy[i] = numpy.sqrt(Dy0[i]**2+(-1./pars[1]*ff(x[i], *pars)*Dx[i])**2)


    # save the plot
    pylab.show()
