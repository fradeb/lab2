import pylab
import numpy
from scipy.optimize import curve_fit

for m in range(0, 16):
    # data load
    filename = "data"+str(m).zfill(2)
    x,Dx,y,Dy=pylab.loadtxt("datifit/"+filename+".txt", unpack=True)

    # scatter plot with error bars
    pylab.errorbar(x,y,Dy,Dx,linestyle = '', color = 'black', marker = '.')

    # bellurie 
    pylab.rc('font',size=18)
    pylab.xlabel('$\Delta V$  [V]')
    pylab.ylabel('$I$  [mA]')
    pylab.minorticks_on()

    # AT THE FIRST ATTEMPT COMMENT FROM HERE TO THE END

    # define the function (linear, in this example)
    def ff(x, aa, bb):
        return aa+bb*x

    # define the initial values (STRICTLY NEEDED!!!)
    init=(0,2)

    # prepare a dummy xx array (with 2000 linearly spaced points)
    xx=numpy.linspace(min(x),max(x),2000)

    # plot the fitting curve computed with initial values
    # AT THE SECOND ATTEMPT THE FOLLOWING LINE MUST BE COMMENTED 
    #pylab.plot(xx,ff(xx,*init), color='blue') 

    # set the error
    sigma=Dy
    w=1/sigma**2

    # call the minimization routine
    pars,covm=curve_fit(ff,x,y,init,sigma, absolute_sigma=False)

    # calculate the chisquare for the best-fit function
    chi2 = ((w*(y-ff(x,*pars))**2)).sum()

    # determine the ndof
    ndof=len(x)-len(init)

    # print results on the console
    print('pars:',pars)
    print('covm:',covm)
    print ('chi2, ndof:',chi2, ndof)

    # plot the best fit curve
    pylab.plot(xx,ff(xx,*pars), color='red') 

    # save the plot
    pylab.savefig("fit_plots/"+filename+".png")
