import pylab
import numpy
from scipy.optimize import curve_fit

# data load
x,Dx,y,Dy=pylab.loadtxt('data07.txt',unpack=True)

# use subplots to display two plots in one figure
# note the syntax
f, (a0, a1) = pylab.subplots(2, 1, gridspec_kw={'height_ratios': [4, 1]})

# scatter plot with error bars
a0.errorbar(x,y,Dy,Dx,linestyle = '', color = 'black', marker = '.')

# bellurie
a0.set_title("Fit numerico")
a0.set_xlabel('$\Delta V$  [V]')
a0.set_ylabel('I  [mA]')
a0.minorticks_on()
a0.grid(linestyle='dotted')

# AT THE FIRST ATTEMPT COMMENT FROM HERE TO THE END

# define the function (linear, in this example)
def ff(x, aa, bb):
    return aa+bb*x

# define the initial values (STRICTLY NEEDED!!!)
init=(0,2)

# prepare a dummy xx array (with 2000 linearly spaced points)
xx=numpy.linspace(min(x),max(x),2000)

# plot the fitting curve computed with initial values
# AT THE SECOND ATTEMPT THE FOLLOWING LINE MUST BE COMMENTED
#pylab.plot(xx,ff(xx,*init), color='blue')

# set the error
for i in range(500):
    sigma=(Dy**2+(Dx*init[0])**2)**0.5
    w=1/sigma**2

    # call the minimization routine
    pars,covm=curve_fit(ff,x,y,init,sigma, absolute_sigma=True)
    init=pars
# calculate the chisquare for the best-fit function
chi2 = ((w*(y-ff(x,*pars))**2)).sum()

# determine the ndof
ndof=len(x)-len(init)

# print results on the console
print('pars:',pars)
print('covm:',covm)
print ('chi2, ndof:',chi2, ndof)

# plot the best fit curve
a0.plot(xx,ff(xx,*pars), color='red')

# build the array of the normalized residuals
r = (y-ff(x,*pars))/sigma

# bellurie
pylab.xlabel('$\Delta V$  [V]')
a1.minorticks_on()
pylab.title("Residui normalizzati")
a1.grid(linestyle='dotted')

# set the vertical range for the norm res
pylab.ylim((-.9,.9))

# plot residuals as a scatter plot with connecting dashed lines
a1.errorbar(x,r,linestyle='',color='blue',marker='.')

f.tight_layout()
# show the plot
pylab.show()
