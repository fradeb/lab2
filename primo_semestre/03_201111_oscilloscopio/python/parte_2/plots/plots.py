import pylab
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

def line(x, m, q):
    return m*x+q

# data load
plt.figure('Grafico valore atteso - valore misurato digitalmente')

ls = np.linspace(0, 10, 100)

tipi = ['seno', 'quadra']
linee = ['-', '--']
fmt = ['s', 'o']


for i in zip(tipi, linee, fmt):
    y, dy = pylab.loadtxt('../digitale/'+i[0]+'_output.txt', unpack=True)
    x, dx = pylab.loadtxt('../'+i[0]+'_atteso.txt', unpack=True)

    plt.errorbar(x, y, dy, dx, fmt = i[2], color = 'black', label = i[0])

    popt, pcov = curve_fit(line, x, y) 

    plt.plot(ls, line(ls, *popt), linestyle = i[1], color = 'gray')

    print(popt)
    print(np.sqrt(pcov.diagonal()))

plt.xlabel("$V_{att}$ [V]")
plt.ylabel("$V$ [V]")
plt.legend(loc='upper left')
plt.savefig('plot.pdf')
