#laboratorio
import pylab
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

def fit(x, a):
    return (1+(x/a)**2)**(-0.5)

def line(x, m, q):
    return m*x+q

x = pylab.loadtxt("f.txt", unpack = True)
v, dv = pylab.loadtxt("a_out.txt", unpack = True)

vin = 20.0
dvin = 0.2

y = v/vin
dy = v/vin*np.sqrt((dv/v)**2+(dvin/vin)**2)

#fit
popt, pcov = curve_fit(fit, x, y, sigma=dy)

#creo il grafico
ls = np.linspace(3, 10e3, 100000)
fig, (ax0, ax1) = plt.subplots(nrows=2, sharex=True)
fig.suptitle('')

#plot sopra
ax0.errorbar(x, y, dy, fmt='o', color='black', markersize='3')

ax0.set_xlabel(r'$f~[$s$^{-1}]$')
ax0.set_ylabel(r'$A_A$')

ax0.set_xscale('log')
ax0.set_yscale('log')

ax0.plot(ls, fit(ls, *popt), color='gray')

#calcolo i residui
res = y - fit(x, *popt)

#plot sotto
ax1.errorbar(x, res, dy, fmt='o', color='black', markersize='3')

ax1.set_xscale('log')

ax1.plot(ls, line(ls, 0, 0), color='gray')

#salvo e mostro il grafico
plt.savefig('a_plot.pdf')
plt.show()
plt.close()
