#laboratorio
import pylab
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

def line(x, m, q):
    return x*m + q

x = pylab.loadtxt("f.txt", unpack = True)
v, dv = pylab.loadtxt("b_out.txt", unpack = True)

vin = 20.0
dvin = 0.2

y = v/vin
dy = v/vin*np.sqrt((dv/v)**2+(dvin/vin)**2)

#creo il grafico
ls = np.linspace(0, 15e3, 100)
fig, ax0 = plt.subplots(nrows=1, sharex=True)
fig.suptitle('')

#fit
popt, pcov = curve_fit(line, x, y, sigma=dy)

#plot sopra
ax0.errorbar(x, y, dy, fmt='o', color='black', markersize='3')

ax0.set_xlabel(r'$f~[$s$^{-1}]$')
ax0.set_ylabel(r'$A_B$')

ax0.set_xscale('log')

print(*popt)
ax0.plot(ls, line(ls, 0, popt[1]), color='gray')

#salvo e mostro il grafico
plt.savefig('b_plot.pdf')
plt.show()
plt.close()
