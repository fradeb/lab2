#laboratorio
import pylab
import numpy as np

#cmd
import sys, getopt

def main(argv):
    inputfile = ''
    outputfile = ''
    try:
        opts, args = getopt.getopt(argv,"i:o:",["ifile=","ofile="])
    except getopt.GetoptError:
        print('digitale.py -i <inputfile> -o <outputfile>')
        sys.exit(2)

    for opt, arg in opts:
        if opt == '-h':
            print('test.py -i <inputfile> -o <outputfile>')
            sys.exit()
        elif opt in ("-i", "--ifile"):
            inputfile = arg
        elif opt in ("-o", "--ofile"):
            outputfile = arg

    dig, perc, last, mult = pylab.loadtxt(inputfile, unpack = True)

    sigma_dig = np.sqrt((last*mult)**2 + (dig*perc/100)**2)

    f = open(outputfile, "w")
    f.write("#value\t\t\t#sigma\n")
    for i in range(0, len(dig)):
        f.write("{:.5e}".format(dig[i]) + "\t\t" + "{:.5e}".format(sigma_dig[i]) + "\n")
    f.close()

if __name__ == "__main__":
   main(sys.argv[1:])
