#laboratorio
import pylab
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

def line(x, m, q):
    return m*x+q

x = pylab.loadtxt("f.txt", unpack = True)
y, dy = pylab.loadtxt("a_out.txt", unpack = True)

#fit
popt, pcov = curve_fit(line, x, y, sigma=dy)

#creo il grafico
ls = np.linspace(0, 1000, 100)
fig, (ax0, ax1) = plt.subplots(nrows=2, sharex=True)
fig.suptitle('')

#plot sopra
ax0.errorbar(x, y, dy, fmt='o', color='black', markersize='3')

ax0.set_xlabel(r'$p^{-1}~[cm^{-1}]$')
ax0.set_ylabel(r'$q^{-1}~[cm^{-1}]$')

ax0.set_xscale('log')
ax0.set_yscale('log')
ax0.plot(ls, line(ls, *popt), color='gray')

#calcolo i residui
res = y - line(x, *popt)

#plot sotto
ax1.errorbar(x, res, dy, fmt='o', color='black', markersize='3')

ax1.set_xscale('log')
ax1.set_yscale('log')

ax1.plot(ls, line(ls, 0, 0), color='gray')

#salvo e mostro il grafico
plt.savefig('grafico.pdf')
plt.show()
plt.close()
