\documentclass{article}
\usepackage[cm]{fullpage}
\usepackage{graphicx}
\usepackage{booktabs}
\usepackage[separate-uncertainty = true]{siunitx}

\renewcommand{\figurename}{Fig.}

\title{Integratore e derivatore}
\date{18/11/2020}
\author{Debortoli Francesco\\Matteri Antonio}

\begin{document}
    \maketitle 

    \begin{itemize}
        \item \textbf{1.}

            $R_{\text{misurata}}=\SI{672 \pm 5}{\Omega}$ \qquad
            $C_{\text{nominale}}=\SI{2.2\pm 0.2}{\mu F}$

            La frequenza di taglio corrispondente a questi dati è $f_T=\SI{126\pm12}{Hz}$. Se $V_{in}$ è un'onda sinusoidale ad alta frequenza
            (cioè molto maggiore della frequenza di taglio), si riceve come $V_{out}$ un'onda sinusoidale sfasata di circa $\frac{\pi}{2}$.
            Ad esempio, si è misurato che alla frequenza di immissione pari a $\SI{1.1048\pm0.0001}{kHz}$, lo sfasamento tra i due segnali
            era di $\SI{196\pm10}{\mu s}$; calcolando l'angolo di sfasamento si ottiene $(0.433\pm0.018)\pi$, non compatibile con $\frac{\pi}{2}$,
            in quanto la frequenza non era sufficientemente alta. A bassa frequenza lo sfasamento diminuisce fino quasi ad annullarsi.

            Con un'onda quadra si nota che il segnale d'ingresso non risulta esattamente un'onda quadra (si nota per frequenze basse), ma risulta
            in modulo leggermente minore in prossimità dei cambi di segno del potenziale del generatore. Questo è dovuto al fatto che, quando cambia
            la tensione, il condensatore è carico al contrario e richiede aal generatore una corrente notevole per caricarsi; questa corrente è tale da rendere
            misurabile la caduta di potenziale dovuta alla resistenza interna del generatore e causa quindi questo effetto. Quantitativamente si può stimare
            che la caduta che si rileva istantaneamente dopo il cambio di segno del potenziale è pari a (chiamo $V_0$ la metà del potenziale picco picco):
            \begin{equation}
                \Delta V = \frac{2V_0r}{R+r}
            \end{equation}
            Che valutato nel nostro caso significa (ricordando che la resistenza interna nominale è $\SI{50}{\Omega}$) che la
            caduta di potenziale è il $14\pm1 \%$ di $V_0$.
            
            Il potenziale di uscita con un'onda quadra è un'onda a forma di pinna di squalo che tende a diventare un'onda triangolare nel
            limite di alte frequenze e un'onda quadra nel limite di basse frequenze, entrambe con il punto angoloso quando cambia il segno
            del potenziale in ingresso.

        \item \textbf{2.}
            
            $R_{misurata}=\SI{72.5 \pm 0.7}{\Omega}$ \qquad
            $C_{nominale}=\SI{0.22\pm 0.02}{\mu F}$

            Sia per l'onda quadra che per l'onda sinusoidale, si ha che $V_{out}$ è la derivata di $V_{in}$ solo per frequenze basse (molto minori di 
            $f_T=\SI{9.98 \pm 0.10}{kHz}$). Nel caso di onda quadra in particolare, si ha che per frequenze molto basse $V_{out}$ è costantemente nulla, tranne che
            per dei picchi in cui il potenziale diventa molto alto in corrispondenza dei cambi di segno dell'onda quadra. Per frequenze prossime alla frequenza di taglio
            si nota che in realtà i picchi sono degli esponenziali che partono da valori molto alti in corrispondenza dei cambi di tensione e hanno un asintoto orizzontale
            in corrispondenza dello $0$. Nel limite di frequenze alte si ha un'onda a dente di sega.

        \item \textbf{5.}
            
            $$f_{TA}=\frac{1}{2\pi R_A C_A}$$
            $$f_{TB}=\frac{1}{2\pi R_B C_B}$$
            La condizione perché $A$ funzioni da integratore e $B$ da derivatore è $f_{TA} \ll f \ll f_{TB}$.

        \item \textbf{6.}
            
            La condizione su $C_A$ e $C_B$ affinché si evitino problemi legati all'interazione dei due circuiti per il calcolo delle frequenze di taglio è, come detto
            a lezione, $C_A>C_B$.

        \item \textbf{7.}
            
            I valori scelti sono:
            $$C_A = \SI{10 \pm 1}{\mu F}$$
            $$R_A = \SI{3.28 \pm 0.03}{k\Omega}$$
            $$C_A = \SI{0.22 \pm 0.02}{\mu F}$$
            $$R_A = \SI{72.5 \pm 0.7}{\Omega}$$
            $$f_{TA} = \SI{4.85 \pm 0.49}{Hz}$$
            $$f_{TB} = \SI{9.98 \pm 0.10}{kHz}$$
            $$f_{scelta}=\SI{275 \pm 1}{Hz}$$

        \item \textbf{8.}
            \begin{center}
            \begin{tabular}{c c c c c c}
                \toprule
                pedice & form d'onda & V & A & A [dB] & $\Delta \varphi [\pi \text{rad}]$ \\
                \midrule
                in & quadra & $\SI{20.0 \pm 0.2}{V}$ & 1 & 0 & 0 \\
                A & triangolare & $\SI{544 \pm 4}{mV}$ & $0.0272 \pm 0.0004$ & $-31.3$  & $0.0777 \pm 0.0008$ \\
                B & quadra & $\SI{10.5 \pm 0.5}{mV}$ & $(5.25\pm 0.27)\cdot 10^{-4}$ & $-65.6$ & $(0.0 \pm 1.4)\cdot 10^{-3}$ \\
                \bottomrule
            \end{tabular}
            \end{center}
            
            In particolare si ha $V_{in} = \SI{86.0}{dBm}$. Le forme d'onda sono quelle che ci si aspettava: un'onda a pinna di squalo
            restituita dall'integratore (degenerata in un'onda triangolare per via della frequenza alta)
            e un'onda quadra dal derivatore. Come ci si poteva aspettare il segnale perde di intensità in ciascuna rilevazione
            (cioè $V_{in}>V_A>V_B$) e inoltre anche la fase è quella attesa: l'onda triangolare è sfasata rispetto alle altre due
            che sono in fase. La fase dell'onda triangolare è stata misurata prendendo la distanza temporale tra quando il potenziale d'ingresso
            vale $0$ e quando $V_A$ assume lo stesso valore e come ci si aspetterebbe questo valore non è nullo in quanto la frequenza non è infinita.

        \item \textbf{9.}
            
            Il rapporto S/N è $5.8\pm 1.7$ senza il condensatore e $21 \pm 9$ con il condensatore. Come si nota il condensatore riduce notevolmente il
            rumore ad alta frequenza presente nel segnale. Il condensatore è collegato in parallelo rispetto all'intero circuito, in modo che la $V_{in}$
            misurata dall'oscilloscopio sia la differenza di potenziale ai capi del condensatore. Se il segnale viene disturbato con del rumore ad alta frequenza
            il condensatore si comporta come l'elemento di un integratore: ignorando cosa succeda al circuito in presenza di tali rumori (si nota comunque che
            per valori alti della frequenza in ingresso il circuito ha una caduta di potenziale dovuta principalmente alle resistenze),
            la resistenza interna dell'oscilloscopio assieme al condensatore forma l'integratore. L'integratore ha una caduta di 
            potenziale dovuta quasi solo alla resistenza per frequenze alte, mentre i segnali a bassa frequenza hanno caduta dovuta principalmente al condensatore.
            La frequenza di taglio stimata ignorando la presenza del resto del circuito è dell'ordine di $f_T=\SI{e2}{Hz}$.

        \item \textbf{11.}
            
            \begin{table}[h!]
                \centering
                \begin{tabular}{ccc}
                    \toprule
                    f [$\si{\Hz}]$&$V_{A}$ [$\si{\V}]$     &$V_{B}$ [$\si{\V}]$\\
                    \midrule
                   $10.14\cdot10^3$        & 0.0168		$\pm$ 0.0009          &9.6	    $\pm$ 0.8\\ 
                    $6.06\cdot10^3$        & 0.025  	$\pm$ 0.001           &10.3		$\pm$ 0.8\\
                    $4.77\cdot10^3$        & 0.031		$\pm$ 0.001           &10.3		$\pm$ 0.8\\
                    $3.06\cdot10^3$        & 0.048		$\pm$ 0.001           &10.3		$\pm$ 0.8\\
                    $2.01\cdot10^3$        & 0.075		$\pm$ 0.002           &10.4		$\pm$ 0.8\\
                    $1.39\cdot10^3$        & 0.106		$\pm$ 0.003           &10.4		$\pm$ 0.8\\
                    $1.00\cdot10^3$        & 0.146		$\pm$ 0.004           &10.4		$\pm$ 0.8\\
                    706                    & 0.210		$\pm$ 0.007           &10.4		$\pm$ 0.8\\
                    407                    & 0.36		$\pm$ 0.01            &10.4		$\pm$ 0.8\\
                    290                    & 0.52		$\pm$ 0.02            &10.4		$\pm$ 0.8\\
                    199                    & 0.77		$\pm$ 0.02            &10.4		$\pm$ 0.8\\
                    130                    & 1.14		$\pm$ 0.04            &10		$\pm$ 1  \\
                    84                     & 1.78		$\pm$ 0.06            &10		$\pm$ 1  \\
                    60                     & 2.50		$\pm$ 0.08            &10		$\pm$ 1  \\
                    40                     & 3.64       $\pm$ 0.1             &9		$\pm$ 2  \\
                    25                     & 5.64       $\pm$ 0.2             &9		$\pm$ 2  \\
                    16                     & 8.24       $\pm$ 0.3             &9		$\pm$ 2  \\
                    10                     & 13.0       $\pm$ 0.4             &9		$\pm$ 2  \\
                    3.4                    & 19.8       $\pm$ 0.7             &9		$\pm$ 2  \\
                    \bottomrule
                \end{tabular}
            \end{table}

            Con un breve controllo si è notato che il valore $V_{in}$ non dipende dalla frequenza del generatore e dunque
            si può considerare costante. In questo caso $V_{in}=\SI{20.0\pm0.2}{V}$. Di seguito si riportano i grafici dei fit
            effettutati. 
            
            Effettuando un fit dei minimi quadrati su $A_A$ con l'equazione data si ottiene $f_{TA}=\SI{7.48\pm0.06}{Hz}$, non compatibile
            con il valore $f_{TA} = \SI{4.85 \pm 0.49}{Hz}$ ottenuto a partire dalla resistenza e dalla capacità degli elementi circuitali.

            Per quanto riguarda $A_B$, si ottiene, effettuando un fit dei minimi quadrati lineare, un coefficiente angolare pari a
            $m=\SI{-1.27\pm1.6 e-9}{dB Hz^{-1}}$ e un'intercetta pari a $q=\SI{5.11\pm0.06 e-4}{dB}$. Il valore del coefficiente angolare dice che
            i punti sono quasi allineati orizzontalmente, del resto dallo schermo dell'oscilloscopio si notava che l'andamento era asintoticamente
            rettilineo orizzontale.

            \begin{figure}[ht!]
                \centering
                \includegraphics[scale= 0.7]{../11/a_plot.pdf}
                \caption{Valori di $A_{A}$ in funzione della frequenza.}
            \end{figure}

            \begin{figure}[ht!]
                \centering
                \includegraphics[scale= 0.7]{../11/b_plot.pdf}
                \caption{Valori di $A_{B}$ in funzione della frequenza.}
            \end{figure}

    \end{itemize}
\end{document}
