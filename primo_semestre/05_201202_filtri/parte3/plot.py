#laboratorio
import pylab
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

from table import*

def func(x, fpb):
    return ((1+(x/fpb)**2))**(-1/2) 

def line(x, m, q):
    return m*x+q

x = pylab.loadtxt("freq.txt", unpack = True)
yin, dyin = pylab.loadtxt("vin_o.txt", unpack = True)
yout, dyout = pylab.loadtxt("vout_o.txt", unpack = True)
y = yout/yin
dy = y*np.sqrt((dyout/yout)**2+(dyin/yin)**2)

#sfasamento
sfasa, dsfasa = pylab.loadtxt("sfasa_o.txt", unpack = True)
delta = 2*sfasa*x
ddelta = 2*dsfasa*x
print(delta)
print(ddelta)

#fit
popt, pcov = curve_fit(func, x, y, sigma=dy, absolute_sigma = True)

#matrice di covarianza normalizzata
pars = 1 
ncov = np.empty((pars, pars))
for i in range(0, pars):
    for j in range(0, pars):
        ncov[i][j] = pcov[i][j]/np.sqrt(pcov[i][i]*pcov[j][j])

print(popt)
print(np.sqrt(pcov.diagonal()))
print(ncov)

#creo il grafico
ls = np.linspace(5, 2e5, 100000)
fig, (ax0, ax1) = plt.subplots(nrows=2, sharex=True)
fig.suptitle('')

#plot sopra
ax0.errorbar(x, y, dy, fmt='o', color='black', markersize='3')

ax0.set_ylabel('A')
ax0.set_xlabel('f [Hz]')

ax0.set_xscale('log')
#ax0.set_yscale('log')
ax0.plot(ls, func(ls, *popt), color='gray')

#calcolo i residui
res = y - func(x, *popt)

#plot sotto
ax1.errorbar(x, res, dy, fmt='o', color='black', markersize='3')

ax1.set_xscale('log')
#ax1.set_yscale('log')

ax1.plot(ls, line(ls, 0, 0), color='gray')

#salvo e mostro il grafico
plt.savefig('grafico.pdf')
plt.show()
plt.close()

#scrivo i dati su tn
data = np.zeros((5, len(x)))
data[0] = x
data[1] = yin
data[2] = yout
data[3] = y
data[4] = delta

sigma = np.zeros((5, len(x)))
sigma[0] = [1e-3]*len(x) 
sigma[1] = dyin
sigma[2] = dyout
sigma[3] = dy
sigma[4] = ddelta

data = data.transpose()
sigma = sigma.transpose()

latex_table(data, sigma, 'output.txt', True)

#scrivo i dati del fit su latex
data = np.zeros((1, 1))
sigma = np.zeros((1, 1))
data[0] = popt
sigma[0] = np.sqrt(pcov.diagonal())

latex_table(data, sigma, 'fit.txt', True)


