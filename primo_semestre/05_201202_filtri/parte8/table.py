import numpy as np
from math import floor, log10

def get_d(x):
	return (-int(floor(log10(x))))
	

def latex_table(data, sigma, nomefile, intestazione):	
	outputFile = open(nomefile, 'w')
	
	if(intestazione):
		m = len(data[0])
		s=r'\begin{table}[H]'+'\n\t'+r'\center'+'\n\t'+r'\begin{tabular}{'
		for i in range(0, m): s+='r'
		s+='}'+'\n\t\t'+r'\toprule'+'\n\t\t'
		for i in range(0, m-1): s+='\t&'
		s+='\t'+r'\\'+'\n\t\t'+r'\midrule'+'\n'
		outputFile.write(s)
		
		for i in range(0, len(data)):
			s = '\t\t\t'
			for j in range(0, len(data[i])):
				s+='%.10f\t$\pm$\t%.10f\t' % (round(data[i][j], get_d(sigma[i][j])), round(sigma[i][j], get_d(sigma[i][j])))
				if (j != len(data[i])-1):
					s+='&\t'
			s+=r'\\'+'\n'
			outputFile.write(s)
			
		s='\t\t'+r'\bottomrule'+'\n\t'+r'\end{tabular}'+'\n'r'\end{table}'
		outputFile.write(s)

	if(not intestazione):
		for i in range(0, len(data)):
			s = ''
			for j in range(0, len(data[i])):
				s+='%.10f\t$\pm$\t%.10f\t' % (round(data[i][j], get_d(sigma[i][j])), round(sigma[i][j], get_d(sigma[i][j])))
				if (j != len(data[i])-1):
					s+='&\t'
			s+=r'\\'+'\n'
			outputFile.write(s)

