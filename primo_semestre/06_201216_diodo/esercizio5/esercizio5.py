import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from table import *

def sparse(l, s):
    res = []
    for i in range(0, len(l), s):
        res.append(l[i])
    res = np.array(res)
    return res

def shock(x, eta, I0, offset):
    VT = 0.025606
    return I0*(np.exp(x/(eta*VT))-1)+offset

def line(x, m, q):
    return m*x+q

tmin = [0.00548, 0.005452, 0.00548, 0.00549, 0.00548]
tmax = [0.00694, 0.0069, 0.00694, 0.00695, 0.00694]
files = ['58', '59', '60', '61', '62']
RD = [327.98, 67.685, 671.98, 3281.8, 327.98]
RD = np.array(RD)

for case in [0, 1, 2, 3, 4]:
    #da stampare
    data = []
    sigma = []

    t, V1, V2 = np.loadtxt('../dati/DSO000'+files[case]+'.CSV', delimiter=',',skiprows=2,usecols=(0,1,2), unpack=True)
    #taglio le parti di salita e discesa
    for i in range(len(t)):
        if(t[i] > tmin[case]):
            imin = i
            break
    for i in range(len(t)):
        if(t[i] < tmax[case]):
            imax = i

    t = t[imin:imax]
    V2 = V2[imin:imax]
    V1 = V1[imin:imax]

    s = 200 
    t = sparse(t, s)
    V1 = sparse(V1, s)
    V2 = sparse(V2, s)


    #calcolo le incertezze    
    perc_lettura = 0.03
    perc_offset = 0.01
    offset1 = 6
    offset2 = 0.400
    cost = 0.002

    dV1 = 0.03*V1
    dV2 = 0.03*V2
    
    dV1 = np.sqrt((perc_lettura*V1)**2+(perc_offset*offset1)**2+cost**2)
    dV2 = np.sqrt((perc_lettura*V2)**2+(perc_offset*offset2)**2+cost**2)

    VD = V2-V1
    dVD = np.sqrt(dV1**2+dV2**2)

    dRD = 0.008*RD

    print(RD)
    print(dRD)

    ID = VD/RD[case]
    dID = ID*np.sqrt((dRD[case]/RD[case])**2+(dVD/VD)**2)

    popt, pcov = curve_fit(shock, V1, ID, p0=[2, 0.01, 0], sigma=dID, absolute_sigma=False)
    data.append(popt)
    sigma.append(np.sqrt(pcov.diagonal()))

    #normalizzo la matrice
    npcov = pcov
    for i in range(len(pcov)):
        for j in range(len(pcov[i])):
            npcov[i][j] = pcov[i][j]/(np.sqrt(pcov.diagonal())[i]*np.sqrt(pcov.diagonal())[j])

    x = V1
    y = ID
    dx = dV1
    dy = dID

    #ZONA GRAFICI
    ls = np.linspace(V1.min(), V1.max(), 1000)
    fig, (ax0, ax1) = plt.subplots(nrows=2, sharex=True)
    fig.suptitle('')

    #plot sopra
    ax0.errorbar(x, y, dy, dx, fmt='o', color='black', markersize='3')

    ax0.set_xlabel('V [V]')
    ax0.set_ylabel('I [A]')

    ax0.plot(ls, shock(ls, *popt), color='gray')

    #calcolo i residui
    res = y - shock(x, *popt)

    #plot sotto
    ax1.errorbar(x, res, dy, dx, fmt='o', color='black', markersize='3')

    ax1.plot(ls, line(ls, 0, 0), color='gray')

    #salvo e mostro il grafico
    plt.savefig('./plot/'+str(case)+'.pdf')
    plt.show()
    plt.close()

    #COPISTERIA
    #ora voglio semplicemente stamparmi i dati da qualche parte
    #custom_intestazione(data, sigma, '../relazione/6_raw.tex', [r'$\eta$', r'$I_0$ [A]', r'offset [A]'])
    #print_matrix(pcov, '../relazione/6_raw.tex')
