import numpy as np
from math import floor, log10

def get_d(x):
    return (-int(floor(log10(x))))


def latex_table(data, sigma, nomefile, intestazione):	
    outputFile = open(nomefile, 'a')

    if(intestazione):
        m = len(data[0])
        s=r'\begin{table}[H]'+'\n\t'+r'\center'+'\n\t'+r'\begin{tabular}{'
        for i in range(0, m): s+='r'
        s+='}'+'\n\t\t'+r'\toprule'+'\n\t\t'
        for i in range(0, m-1): s+='\t&'
        s+='\t'+r'\\'+'\n\t\t'+r'\midrule'+'\n'
        outputFile.write(s)

        for i in range(0, len(data)):
            s = '\t\t\t'
            for j in range(0, len(data[i])):
                s+='%.10f\t$\pm$\t%.10f\t' % (round(data[i][j], get_d(sigma[i][j])), round(sigma[i][j], get_d(sigma[i][j])))
                if (j != len(data[i])-1):
                    s+='&\t'
            s+=r'\\'+'\n'
            outputFile.write(s)

        s='\t\t'+r'\bottomrule'+'\n\t'+r'\end{tabular}'+'\n'r'\end{table}'
        outputFile.write(s)
        outputFile.write('\n\n')

    if(not intestazione):
        for i in range(0, len(data)):
            s = ''
            for j in range(0, len(data[i])):
                s+='%.10f\t$\pm$\t%.10f\t' % (round(data[i][j], get_d(sigma[i][j])), round(sigma[i][j], get_d(sigma[i][j])))
                if (j != len(data[i])-1):
                    s+='&\t'
            s+=r'\\'+'\n'
            outputFile.write(s)
        outputFile.write('\n\n')

def custom_intestazione(data, sigma, nomefile, custom):	
    outputFile = open(nomefile, 'a')
    m = len(data[0])
    s=r'\begin{table}[H]'+'\n\t'+r'\center'+'\n\t'+r'\begin{tabular}{'
    for i in range(0, m): s+='r'
    s+='}'+'\n\t\t'+r'\toprule'+'\n\t\t'
    s+=custom[0]+'\t'
    for c in range(1, len(custom)): s+='&'+custom[c]+'\t'
    s+='\t'+r'\\'+'\n\t\t'+r'\midrule'+'\n'
    outputFile.write(s)

    for i in range(0, len(data)):
        s = '\t\t\t'
        for j in range(0, len(data[i])):
            s+='%.10f\t$\pm$\t%.10f\t' % (round(data[i][j], get_d(sigma[i][j])), round(sigma[i][j], get_d(sigma[i][j])))
            if (j != len(data[i])-1):
                s+='&\t'
        s+=r'\\'+'\n'
        outputFile.write(s)

    s='\t\t'+r'\bottomrule'+'\n\t'+r'\end{tabular}'+'\n'r'\end{table}'
    outputFile.write(s)
    outputFile.write('\n\n')

def print_matrix(data, nomefile):	
    outputFile = open(nomefile, 'a')
    s=r'\begin{equation*}'+'\n'+r'\begin{bmatrix}'+'\n'
    for i in range(len(data)):
        s+=str(data[i][0])+'\t\t\t'
        for j in range(1, len(data[i])):
            s+='&'+str(data[i][j])+'\t\t\t'
        if(i != len(data)-1):
            s+=r'\\'
        s+='\n'
    s+=r'\end{bmatrix}'+'\n'+r'\end{equation*}'
    outputFile.write(s)
    outputFile.write('\n\n')

data = 2.98066
sigma = 3.5725e-3
print(get_d(sigma))
print(round(sigma, get_d(sigma)))
