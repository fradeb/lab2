import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

#I dati da prendere
t, x, y, z = np.loadtxt('../dati/DSO00063.CSV', delimiter=',', skiprows=2, usecols=(0,1,2,3), unpack=True)

x=x-np.mean(x)
#Cerco il semiperiodo dell'onda quadra guardando quando cambia segno
tmp=np.sign(x[0])
ts=np.zeros(0)
ns=np.zeros(0)

for i in range(len(t)):
    if tmp==-1 and x[i]>0:
        ts=np.append(ts,t[i])
        ns=np.append(ns,i)
        tmp=1
    elif tmp==1 and x[i]<0:
        ts=np.append(ts,t[i])
        ns=np.append(ns,i)
        tmp=-1
    elif tmp==0:
        tmp=np.sign(x[i])
#ns=list(ns)
#ns.pop(4)
#ns.pop(3)
#ns.pop(18)
#ns.pop(17)
#ns=np.array(ns)
#ts=list(ts)
#ts.pop(4)
#ts.pop(3)
#ts.pop(18)
#ts.pop(17)
#ts=np.array(ts)
#print(ns)

delta=np.zeros(len(ts)-1)
deltan=np.zeros(len(ts)-1)
for i in range(len(ns)-1):
    delta[i]=ts[i+1]-ts[i]
    deltan[i]=ns[i+1]-ns[i]

#la media è il semiperiodo
T, dT = 2*np.mean(delta), 2*np.std(delta)
print(str(T)+'+/-'+str(dT))

#Faccio una media dei vari periodi e la sistemo in maniera da centrarla
dt = t[1]-t[0]
N = int(np.mean(deltan))
print(N)
m = np.zeros(2*N)
n = np.zeros(2*N)
o = np.zeros(2*N)
tmp = np.zeros(2*N)

for i in range(len(t)):
    j = int(i%(2*N)-ns[0])
    if j<0:
        j+= 2*N
    m[j]+=y[i]
    n[j]+=x[i]
    o[j]+=z[i]
    tmp[j]+=1

for i in range(2*N):
    m[i]/=tmp[i]
    n[i]/=tmp[i]
    o[i]/=tmp[i]

print(tmp[10])
#Trovo la Vpp
Vpp=np.max(n)-np.min(n)
print('VG:',Vpp)

Vd=np.max(m)-np.min(m)
print('Vd:',Vd)

plt.plot(range(2*N),n)
plt.figure('a')
plt.plot(range(2*N),m)
#plt.plot(range(2*N),o)
plt.show()
