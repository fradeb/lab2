import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

#I dati da prendere
t, x, y = np.loadtxt('../dati/DSO00048.CSV', delimiter=',', skiprows=2, usecols=(0,1,2), unpack=True)
#t, x = np.loadtxt('../dati/DSO000'+str(case).rjust(2, '0')+'.CSV', delimiter=',', skiprows=2, usecols=(0,1), unpack=True)


#Cerco il semiperiodo dell'onda quadra guardando quando cambia segno
tmp=np.sign(x[0])
ts=np.zeros(0)
ns=np.zeros(0)

for i in range(len(t)):
    if tmp==-1 and x[i]>0:
        ts=np.append(ts,t[i])
        ns=np.append(ns,i)
        tmp=1
    elif tmp==1 and x[i]<0:
        ts=np.append(ts,t[i])
        ns=np.append(ns,i)
        tmp=-1
    elif tmp==0:
        tmp=np.sign(x[i])

delta=np.zeros(len(ts)-1)
deltan=np.zeros(len(ts)-1)
for i in range(len(ts)-1):
    delta[i]=ts[i+1]-ts[i]
    deltan[i]=ns[i+1]-ns[i]

#la media è il semiperiodo
T, dT = 2*np.mean(delta), 2*np.std(delta)
#print(str(T)+'+/-'+str(dT))

#Faccio una media dei vari periodi e la sistemo in maniera da centrarla
dt = t[1]-t[0]
N = int(np.mean(deltan))
print(N, ns[0])

#ns = np.array([0])
#N, ns[0] = [781, 126.0]
m = np.zeros(2*N)
n = np.zeros(2*N)
tmp = np.zeros(2*N)

for i in range(len(t)):
    j = int(i%(2*N)-ns[0])
    if j<0:
        j+= 2*N
    m[j]+=y[i]
    n[j]+=x[i]
    tmp[j]+=1

for i in range(2*N):
    m[i]/=tmp[i]
    n[i]/=tmp[i]

#Trovo la Vpp
Vpp=np.max(m)-np.min(m)
print(Vpp)

#Disegna il seno
R=3300
C=4.7*10**-6
y1=Vpp*np.sin(1/T*t)
y2=Vpp*np.exp(-t/(R*C))



plt.figure(str(case))
plt.plot(range(2*N),m)
plt.show()
