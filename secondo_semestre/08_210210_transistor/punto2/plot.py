#laboratorio
import pylab
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

def line(x, m, q):
    return m*x+q

def fit(x, m, q):
    return m*x+q

#x, dx = pylab.loadtxt("i_b_output.txt", unpack = True)
#y, dy = pylab.loadtxt("i_c_output.txt", unpack = True)
x, dx = pylab.loadtxt("i_b_tagliato_output.txt", unpack = True)
y, dy = pylab.loadtxt("i_c_tagliato_output.txt", unpack = True)

#fit
popt, pcov = curve_fit(fit, x, y, sigma=dy, absolute_sigma=False)
print(popt)
print(np.sqrt(pcov.diagonal()))
print(pcov)

#creo il grafico
ls = np.linspace(0, 40, 1000)
fig, (ax0, ax1) = plt.subplots(nrows=2, sharex=True)
fig.suptitle('')

#plot sopra
ax0.errorbar(x, y, dy, fmt='o', color='black', markersize='3')

ax0.set_ylabel(r'$I_C$ [mA]')
ax0.set_xlabel(r'$I_B$ [μA]')

ax0.plot(ls, fit(ls, *popt), color='gray')

#calcolo i residui
res = (y - fit(x, *popt))/dy

#plot sotto
ax1.errorbar(x, res, fmt='o', color='black', markersize='3')

ax1.plot(ls, line(ls, 0, 0), color='gray')

#salvo e mostro il grafico
plt.savefig('fit.pdf')
plt.show()
plt.close()
