#laboratorio
import pylab
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

def line(x, m, q):
    return m*x+q

def fit(x, m, q):
    return np.exp(m*x+q) 

x, dx = pylab.loadtxt("i_b_output.txt", unpack = True)
y, dy = pylab.loadtxt("i_c_output.txt", unpack = True)

#creo il grafico
ls = np.linspace(0, 1000, 100)
fig, (ax0) = plt.subplots(nrows=1, sharex=True)
fig.suptitle('')

#plot sopra
ax0.errorbar(x, y, dy, fmt='o', color='black', markersize='3')

ax0.set_ylabel(r'$I_C$ [mA]')
ax0.set_xlabel(r'$I_B$ [μA]')

#salvo e mostro il grafico
plt.savefig('tutti_i_dati.pdf')
plt.show()
plt.close()
