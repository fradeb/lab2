import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

def media(v):
    vn = np.array([])
    for i in range(1,len(v),7):
        j=5
        if i<5:
            j = i
        if len(v)-5<i:
            j = len(v)-i
        vn = np.append(vn,np.mean(v[i-j:i+j]))
    return vn

RC = [989,989,989,989,989,989,989,989,989,2190,2190,2190,2190,2190,2190,2190]
for i in range(1,17):
    print(i)
    t, v1, v2 = np.loadtxt('../dati/DSO000'+str(0 if i<10 else '')+str(i)+'.CSV', skiprows=2, usecols=(0,1,2), unpack=True, delimiter=',')
    v1 = media(v1)
    errv1 = 0.03*v1
    v2 = media(v2)
    I = 1000*(v2-v1)/RC[i-1]
    errI = 0.03*I
    plt.figure(str(i))
    plt.xlabel('V_CE [V]')
    plt.ylabel('I_C [mA]')
    plt.errorbar(v1,I,errI,errv1, fmt='.', color='black')
    plt.savefig('./plots/'+str(i-1)+'.pdf')

def exp(x,a,k):
    return a*np.e**(k*x)

t, v1, v2 = np.loadtxt('../dati/DSO00017.CSV', skiprows=2, usecols=(0,1,2), unpack=True, delimiter=',')
v1 = media(v1)
errv1 = 0.03*v1
v2 = media(v2)
I = 1000*(v2-v1)/67000
errI = 0.03*I
res, cov = curve_fit(exp, v1,I,absolute_sigma=False)
print(res)
print(cov)
plt.figure(str(17))
plt.xlabel('V_B [V]')
plt.ylabel('I_B [mA]')
x=np.linspace(min(v1),max(v1),100)
plt.errorbar(v1,I,errI,errv1, fmt='.', color='red')
plt.plot(x,exp(x,res[0],res[1]),color='black')
plt.savefig('./plots/16.pdf')

#plt.show()

