#laboratorio
import pylab
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from scipy.fft import fft, fftfreq

def line(x, m, q):
    return m*x+q

t, x, y = np.loadtxt('./dati/DSO00038.CSV', delimiter=',',skiprows=2,usecols=(0,1,2),   unpack=True)

#creo il grafico
ls = np.linspace(0, 1000, 100)
fig, (ax0) = plt.subplots(nrows=1, sharex=True)
fig.suptitle('')

#plot sopra
ax0.errorbar(t, x, fmt='o', color='black', markersize='3')

#salvo e mostro il grafico
plt.savefig('grafico.pdf')
plt.show()
plt.close()


yf = fft(x)
xf = fftfreq(len(x), t[1]-t[0])

plt.plot(xf, np.abs(yf))
plt.show()



