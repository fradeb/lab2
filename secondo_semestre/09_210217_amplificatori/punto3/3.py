import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

def lin(x,m,q):
    return m*x+q

ib=np.array([17.79,18.09,18.5,18.62,18.93,19.26,19.58,20.4])
db=np.array([0.4]*7+[0.2])
ic=np.array([3.00,3.09,3.22,3.25,3.32,3.40,3.47,3.64])
dc=np.array([0.02]*8)
a=[1]
for i in range(50):
    e=dc+a[0]*db
    a, da = curve_fit(lin, ib, ic, sigma=e)

x=np.linspace(min(ib),max(ib),100)
f, (a0, a1) = plt.subplots(2, 1, gridspec_kw={'height_ratios': [4, 1]})
a0.errorbar(ib, ic, dc, db, fmt='.',color='red')
a0.plot(x,lin(x,a[0],a[1]),color='black')
a0.grid('dashed',which='both')
a1.set_title("Residui normalizzati",size=11)
ax=f.add_subplot(111, frameon=False)
ax.set_xticks([])
ax.set_yticks([])
a1.errorbar(ib,list((ic[i]-lin(ib[i],a[0],a[1]))/dc[i] for i in range(len(ib))),1,db,fmt='.',color='black')
a1.grid('dashed',which='both')
f.tight_layout()
plt.xlabel('I_B [$\mu$A]',labelpad=20)
plt.ylabel('I_C [mA]',labelpad=25)
plt.grid(ls='dashed')
plt.savefig('./plot.pdf')
plt.show()

