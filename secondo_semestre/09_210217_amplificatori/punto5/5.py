import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit


f=np.array([600,600,600,6000,60000,120000,600,600])

for case in range(33,41):
    t, x, y = np.loadtxt('../dati/DSO000'+str(0 if case<10 else '')+str(case)+'.CSV', delimiter=',',skiprows=2,usecols=(0,1,2),   unpack=True)

    tmp=np.sign(x[0])

    for i in range(len(t)):
        if tmp==-1 and x[i]>0:
            ns=i
            tmp=1
            break
        elif tmp==1 and x[i]<0:
            ns=i
            tmp=-1
            break
        elif tmp==0:
            tmp=np.sign(x[i])

    dt = t[1]-t[0]
    N = int(1/f[case-33]/dt)
    n = [0]*N*2
    m = [0]*N*2
    tmp = [0]*N*2
    for i in range(len(t)):
        j = int(i%(2*N)-ns)
        if j<0:
            j+= 2*N
        m[j]+=y[i]
        n[j]+=x[i]
        tmp[j]+=1

    for i in range(2*N):
        m[i]/=tmp[i]
        n[i]/=tmp[i]
    n=np.array(n)
    m=np.array(m)
    plt.figure(str(case))
    plt.plot(range(2*N),100*n, label="$v_{in}$*100")
    plt.plot(range(2*N),m, label="$v_{out}$")
    plt.plot(range(2*N),np.array(x[:2*N])*100, label="$v_{in}$*100")
    plt.plot(range(2*N),y[:2*N],label="$v_{out}$")
    plt.xlabel("Tempo [s]")
    plt.ylabel("Ampiezza [V]")
    plt.legend()
    plt.savefig(f'./plot/{case}.pdf')
    plt.show()

