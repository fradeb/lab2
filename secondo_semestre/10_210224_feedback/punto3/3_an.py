import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from uncertainties import unumpy as unp
import cmath

f, vin, dvin, vout, dvout, t, dt = np.loadtxt('./misure_punto3_analogico.txt', unpack=True, usecols=(0,1,2,3,4,5,6), skiprows=2)
vin = unp.uarray(vin,dvin)
vout = unp.uarray(vout,dvout)
t = unp.uarray(t,dt)

A = vout/vin*1000
phase = f*t*360/1000

def guad(f, ft1, bf):
    return abs(bf/(1+1j*f*ft1))

def sfas(f, ft1, bf):
    return [180+cmath.phase(complex(bf/(1+1j*f[i]*ft1)))*180/np.pi for i in range(len(f))]

res, dres = curve_fit(guad, list(f), [i.n for i in A],sigma=[i.s for i in A])
print(res, dres)
x=np.logspace(1,4,100)

fig, (a0, a1) = plt.subplots(2, 1, gridspec_kw={'height_ratios': [4, 1]})
a0.errorbar(f, [i.n for i in A], [i.s for i in A], fmt='.',color='red')
a0.plot(x,guad(x,res[0],res[1]),color='black')
a0.grid('dashed',which='both')

a0.set_xscale('log')
a0.set_yscale('log')
a1.set_title("Residui normalizzati",size=11)
ax=fig.add_subplot(111, frameon=False)
ax.set_xticks([])
ax.set_yticks([])
a0.set_xscale('log')
a0.set_yscale('log')
a1.errorbar(f,list((A[i].n-guad(f[i],res[0],res[1]))/A[i].s for i in range(len(f))),1,fmt='.',color='black')
a1.grid('dashed',which='both')
a1.set_xscale('log')
fig.tight_layout()
plt.xlabel('f [Hz]',labelpad=20)
plt.ylabel('A_v',labelpad=25)
plt.grid(ls='dashed')
plt.savefig('./plot/an1.pdf')

res, dres = curve_fit(sfas, f[2:], [i.n for i in phase[2:]],sigma=[i.s for i in phase[2:]], p0=[0.68,54])
print(res, dres)

fig, (a0, a1) = plt.subplots(2, 1, gridspec_kw={'height_ratios': [4, 1]})
a0.errorbar(f[2:], [i.n for i in phase[2:]], [i.s for i in phase[2:]], fmt='.',color='red')
a0.plot(x,sfas(x,res[0],res[1]),color='black')
a0.grid('dashed',which='both')

a1.set_title("Residui normalizzati",size=11)
ax=fig.add_subplot(111, frameon=False)
ax.set_xticks([])
ax.set_yticks([])
a1.errorbar(f[2:],list((phase[2:][i].n-sfas([f[2:][i]],res[0],res[1])[0])/phase[2:][i].s for i in range(len(f[2:]))),1,fmt='.',color='black')
a1.grid('dashed',which='both')
fig.tight_layout()
plt.xlabel('f [Hz]',labelpad=20)
plt.ylabel('$\Delta\phi$ [°]',labelpad=25)
plt.grid(ls='dashed')
plt.savefig('./plot/an2.pdf')
plt.show()

for i in range(len(f)):
    print('$',f[i],'\pm1$ & $',vin[i].n,'\pm',vin[i].s,'$ & $', vout[i].n,'\pm',vout[i].s,'$ & $',t[i].n,'\pm',t[i].s,'\\\\')


