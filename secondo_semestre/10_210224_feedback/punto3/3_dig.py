import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from uncertainties import unumpy as unp
import cmath

f, vin, vout, phase = np.loadtxt('./misure_punto3_digitale.txt', unpack=True, usecols=(0,1,2,3), skiprows=3)
vin = unp.uarray(vin,0.03*vin)
vout = unp.uarray(vout,0.03*vout)
phase = unp.uarray(phase,0.03*phase)

A = vout/vin

def guad(f, ft1, bf):
    return abs(bf/(1+1j*f*ft1))

def sfas(f, ft1, bf):
    return [180+cmath.phase(complex(bf/(1+1j*f[i]*ft1)))*180/np.pi for i in range(len(f))]

res, dres = curve_fit(guad, list(f), [i.n for i in A],sigma=[i.s for i in A])
print(res, dres)
x=np.logspace(1,4,100)

fig, (a0, a1) = plt.subplots(2, 1, gridspec_kw={'height_ratios': [4, 1]})
a0.errorbar(f, [i.n for i in A], [i.s for i in A], fmt='.',color='red')
a0.plot(x,guad(x,res[0],res[1]),color='black')
a0.grid('dashed',which='both')

a0.set_xscale('log')
a0.set_yscale('log')
a1.set_title("Residui normalizzati",size=11)
ax=fig.add_subplot(111, frameon=False)
ax.set_xticks([])
ax.set_yticks([])
a0.set_xscale('log')
a0.set_yscale('log')
a1.errorbar(f,list((A[i].n-guad(f[i],res[0],res[1]))/A[i].s for i in range(len(f))),1,fmt='.',color='black')
a1.grid('dashed',which='both')
a1.set_xscale('log')
fig.tight_layout()
plt.xlabel('f [Hz]',labelpad=20)
plt.ylabel('A_v',labelpad=25)
plt.grid(ls='dashed')
plt.savefig('./plot/dig1.pdf')

res, dres = curve_fit(sfas, f, [i.n for i in phase],sigma=[i.s for i in phase])
print(res, dres)

fig, (a0, a1) = plt.subplots(2, 1, gridspec_kw={'height_ratios': [4, 1]})
a0.errorbar(f, [i.n for i in phase], [i.s for i in phase], fmt='.',color='red')
a0.plot(x,sfas(x,res[0],res[1]),color='black')
a0.grid('dashed',which='both')

a1.set_title("Residui normalizzati",size=11)
ax=fig.add_subplot(111, frameon=False)
ax.set_xticks([])
ax.set_yticks([])
a1.errorbar(f,list((phase[i].n-sfas([f[i]],res[0],res[1])[0])/phase[i].s for i in range(len(f))),1,fmt='.',color='black')
a1.grid('dashed',which='both')
fig.tight_layout()
plt.xlabel('f [Hz]',labelpad=20)
plt.ylabel('$\Delta\phi$ [°]',labelpad=25)
plt.grid(ls='dashed')
plt.savefig('./plot/dig2.pdf')
plt.show()

for i in range(len(f)):
    print('$',f[i],'\pm1$ & $',vin[i].n,'\pm',vin[i].s,'$ & $', vout[i].n,'\pm',vout[i].s,'$ & $',phase[i].n,'\pm',phase[i].s,'$\\\\')
