#laboratorio
import pylab
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from scipy.fft import fft, fftfreq

plt.rcParams['font.size'] = '7'

lunghi = [84, 86, 88, 90, 92, 94, 95]
corti = [83, 85, 87, 89, 91, 93, 96]
I_B = [10.4, 11.1, 14.9, 18.9, 23.5, 27.9, 29.8]

for i in range(len(lunghi)):
    #creo il grafico
    ls = np.linspace(0, 1000, 100)
    fig, (ax0, ax1) = plt.subplots(nrows=2, sharex=False)
    fig.suptitle('$I_B=$'+str(I_B[i])+'µA')

    #plot sopra
    t, x = np.loadtxt('../dati/DSO000'+str(corti[i])+'.CSV', delimiter=',',skiprows=2,usecols=(0,1), unpack=True)

    ax0.errorbar(t, x, fmt='o', color='black', markersize='1')

    ax0.set_xlabel('t [s]')
    ax0.set_ylabel('$v_{in}$ [V]')

    #plot sotto
    t, x = np.loadtxt('../dati/DSO000'+str(lunghi[i])+'.CSV', delimiter=',',skiprows=2,usecols=(0,1), unpack=True)
    yf = fft(x)
    xf = fftfreq(len(x), t[1]-t[0])

    ax1.plot(xf, np.abs(yf))

    ax1.set_xlabel('f [Hz]')
    ax1.set_ylabel('[u.a.]')
    ax1.set_yscale('log')
    ax1.set_xbound(lower=-1500, upper=1500)

    #salvo e mostro il grafico
    plt.savefig('plot/'+str(i)+'.pdf')
    #plt.show()
    plt.close()


