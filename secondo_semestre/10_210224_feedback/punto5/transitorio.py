#laboratorio
import pylab
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from scipy.fft import fft, fftfreq

plt.rcParams['font.size'] = '9'

trans = [97, 98, 99]
I_B = [21.2, 21.2, 21.2]
up = [0.100, 0.070, 0.030]

for i in range(len(trans)):
    #creo il grafico
    ls = np.linspace(0, 1000, 100)
    fig, (ax0) = plt.subplots(nrows=1, sharex=False)
    fig.suptitle('$I_B=$'+str(I_B[i])+'µA')

    #plot sopra
    t, x = np.loadtxt('../dati/DSO000'+str(trans[i])+'.CSV', delimiter=',',skiprows=2,usecols=(0,1), unpack=True)

    ax0.errorbar(t, x, fmt='o', color='black', markersize='1')

    ax0.set_xlabel('t [s]')

    ax0.set_xbound(lower=0, upper=up[i])
    ax0.set_ylabel('$v_{in}$ [V]')

    #salvo e mostro il grafico
    plt.savefig('plot/trans_'+str(i)+'.pdf')
    #plt.show()
    plt.close()


