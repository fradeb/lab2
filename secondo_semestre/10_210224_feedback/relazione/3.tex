Il caso con $C_F$ molto grande è equivalente a quello in cui tale condensatore non è
presente e si riconduce all'esperienza precedente. Inserendo il condensatore in una condizione
in cui il suo effetto sia rilevabile (significa a una frequenza abbastanza alta), si nota che il
guadagno dell'amplificatore è minore e così anche l'ampiezza di $v_{in}$.
Con una breve stima si capisce il perché: supponiamo $I_B\approx\SI{20}{\mu A}$
da cui si ottiene $r_b\approx\SI{1.5}{k\Omega}$, per cui la caduta di potenziale
su $R_B\approx\SI{0.5}{k\Omega}$ è circa $\frac{v_{in}}{4}$ e la corrente che vi passa
è circa $\frac{v_{in}}{4R_B}$. Per quanto riguarda la corrente che passa sul condensatore,
essa è $\left|\frac{v_{out}-v_{in}}{Z_F}\right|\approx A v_{in}2\pi f C_F$. A una frequenza
non troppo alta $f\approx \SI{600}{Hz}$ si avrebbe già che la corrente che passa per il condensatore
sarebbe circa $20$ volte quella che passa per $R_B$ se il guadagno restasse lo stesso ($A\approx50$). In particolare
la corrente che scorre nel condensatore fa diminuire $v_{in}$, che fa diminuire $v_{out}$ che diminuisce la corrente
nel condensatore. Questo causa un effetto di feedback negativo che riduce l'ampiezza di $v_{in}$.

Quando in seguito si inserisce la resistenza maggiore come $R_C$ si nota che $v_{in}$ non è più sinusoidale, questo
perché il circuito inizia a richiedere molta corrente a una tensione elevata (c'è un partitore, quindi il generatore ha una ddp
generata molto più alta di $v_{in}$) e in alcuni pezzi del periodo dell'onda il generatore non è in grado di fornirla, causando quindi la
deformazione del segnale.

Sono stati misurati i seguenti dati al variare della frequenza dell'onda in ingresso:
\begin{center}
\begin{tabular}{ccccc}
    \toprule
    $f [\si{Hz}]$ & $v_{in} [\si{mV}]$ & $v_{out} [\si{mV}]$ & $t [\si{ms}]$ \\
    \midrule
    $ 10.020 \pm0.001$ & $ 22 \pm 2 $ & $ 1.30 \pm 0.06 $ & / \\
    $ 20.007 \pm0.001$ & $ 20 \pm 2 $ & $ 1.16 \pm 0.05 $ & / \\
    $ 40.55 \pm0.01$ & $ 30 \pm 2 $ & $ 1.64 \pm 0.07 $ & $ 13.0 \pm 1.0$ \\
    $ 80.65 \pm0.01$ & $ 16 \pm 2 $ & $ 0.94 \pm 0.05 $ & $ 6.5 \pm 0.5$ \\
    $ 164.0 \pm0.1$ & $ 9.5 \pm 0.9 $ & $ 0.48 \pm 0.03 $ & $ 3.44 \pm 0.12$ \\
    $ 312.3 \pm0.1$ & $ 6.5 \pm 0.7 $ & $ 0.260 \pm 0.009 $ & $ 1.9 \pm 0.07$ \\
    $ 637 \pm1$ & $ 5.8 \pm 0.9 $ & $ 0.141 \pm 0.005 $ & $ 0.98 \pm 0.06$ \\
    $ 1266 \pm1$ & $ 5.2 \pm 0.9 $ & $ 0.077 \pm 0.004 $ & $ 0.48 \pm 0.03$ \\
    $ 2420 \pm1$ & $ 5.0 \pm 0.9 $ & $ 0.041 \pm 0.003 $ & $ 0.31 \pm 0.01$ \\
    $ 4861 \pm1$ & $ 8.0 \pm 0.7 $ & $ 0.026 \pm 0.002 $ & $ 0.15 \pm 0.01$ \\
    $ 9560 \pm1$ & $ 8.0 \pm 0.7 $ & $ 0.014 \pm 0.002 $ & $ 0.083 \pm 0.005$ \\
    \bottomrule
\end{tabular}
\end{center}

Calcolando i guadagni corrispondenti come rapporto di $v_{out}$ e $v_{in}$, si può effettuare un fit
di questi valori in funzione della frequenza. Ci si aspetta un comportamento simile a un passa-basso:
\[A=\frac{c}{\sqrt{1+\tau^2 f^2}}\]

I risultati del fit sono i seguenti:
\[c=58\pm1\]
\[\tau=\SI{3.4\pm 0.1}{ms}\]

Con una matrice di covarianza normalizzata pari a (absolute\_sigma=False):
\begin{equation*}
\begin{bmatrix}
    1       & -0.651\\
    -0.651  & 1    \\
\end{bmatrix}
\end{equation*}
Il grafico corrispondente è il seguente:
\begin{center}
    \begin{figure}[H]
     \begin{center}
         \includegraphics[scale=0.7]{../punto3/plot/an1.pdf}
     \end{center}
     \end{figure}
\end{center}
I risultati non sono compatibili con quelli attesi (in particolare $\tau_{att}=R_CC_F=\SI{0.47\pm0.05}{ms}$), per quanto comunque il modello sembri fittare molto bene i dati.

Dalla differenza temporale tra i picchi dei dati è stato ricavata la differenza di fase dei segnali.
È stato anch'esso oggetto di un fit con la funzione:
\[\Delta\phi=180^{\circ}+\arg\left(\frac{c}{1+j f \tau}\right)\]
Dove $\arg(z)$ è l'argomento del numero complesso $z$.
La funzione è quella che ci si aspetta funzioni per un filtro passa-basso.

I risultati del fit sono:
\[c=12.0\pm0.8\]
\[\tau=\SI{2.0\pm0.3}{ms}\]
Con una matrice di covarianza normalizzata pari a (absolute\_sigma=False):
\begin{equation*}
\begin{bmatrix}
    1       & -0.568\\
    -0.568  & 1    \\
\end{bmatrix}
\end{equation*}

Il grafico corrispondente è il seguente:
\begin{center}
    \begin{figure}[H]
     \begin{center}
         \includegraphics[scale=0.7]{../punto3/plot/an2.pdf}
     \end{center}
     \end{figure}
\end{center}
Come prima il valore misurato per $\tau$ non è compatibile con quello atteso.

Allo stesso modo sono stati effettuati dei fit con i dati ricavati tramite l'oscilloscopio digitale:
\begin{center}
    \begin{tabular}{cccc}
    \toprule
    $f [\si{Hz}]$ & $v_{in} [\si{mV}]$ & $v_{out} [\si{V}]$ & $\Delta\phi [^{\circ}]$ \\
    \midrule
    $ 10.43 \pm0.01$ & $ 23.6 \pm 0.7 $ & $ 1100 \pm 33 $ & $ 190 \pm 5 $\\
    $ 20.36 \pm0.01$ & $ 21.6 \pm 0.6 $ & $ 1060 \pm 32 $ & $ 190 \pm 5 $\\
    $ 41.19 \pm0.01$ & $ 14.6 \pm 0.4 $ & $ 772 \pm 23 $ & $ 190 \pm 5 $\\
    $ 81.65 \pm0.01$ & $ 9.9 \pm 0.3 $ & $ 472 \pm 14 $ & $ 198 \pm 6 $\\
    $ 159.0 \pm0.1$ & $ 9.3 \pm 0.3 $ & $ 416 \pm 12 $ & $ 213 \pm 6 $\\
    $ 302.9 \pm0.1$ & $ 8.7 \pm 0.3 $ & $ 320 \pm 10 $ & $ 226 \pm 7 $\\
    $ 600   \pm1$ & $ 9.1 \pm 0.3 $ & $ 214 \pm 6 $ & $ 246 \pm 7 $\\
    $ 1200  \pm1$ & $ 8.5 \pm 0.3 $ & $ 108 \pm 3 $ & $ 268 \pm 8 $\\
    $ 2411  \pm1$ & $ 8.5 \pm 0.3 $ & $ 54 \pm 2 $ & $ 270 \pm 8 $\\
    $ 4933  \pm1$ & $ 8.8 \pm 0.3 $ & $ 28 \pm 1 $ & $ 277 \pm 8 $\\
    $ 9400  \pm1$ & $ 9.0 \pm 0.3 $ & $ 16.0 \pm 0.5 $ & $ 277 \pm 8 $\\
    \bottomrule
    \end{tabular}
\end{center}
Sono stati poi effettuati fit identici a quelli precedenti. Per quanto riguarda i guadagni è
stato ottenuto:
\[c=49.5\pm0.8\]
\[\tau=\SI{3.10\pm0.08}{ms}\]
E la matrice di covarianza normalizzata (absolute\_sigma=False):
\begin{equation*}
\begin{bmatrix}
    1       & 0.756\\
    0.756  & 1    \\
\end{bmatrix}
\end{equation*}

Il grafico con la curva di best-fit è il seguente:
\begin{center}
    \begin{figure}[H]
    \begin{center}
        \includegraphics[scale=0.7]{../punto3/plot/dig1.pdf}
    \end{center}
    \end{figure}
\end{center}

Mentre per gli sfasamenti i risultati sono:
\[c=0.715\pm0.4\]
\[\tau=\SI{4.1\pm 0.6}{ms}\]
Con una matrice di covarianza normalizzata (absoluta\_sigma=False):
\begin{equation*}
\begin{bmatrix}
    1       & 0.550\\
    0.550  & 1    \\
\end{bmatrix}
\end{equation*}

Il grafico con la curva di best-fit è il seguente:
\begin{center}
    \begin{figure}[H]
    \begin{center}
        \includegraphics[scale=0.7]{../punto3/plot/dig2.pdf}
    \end{center}
    \end{figure}
\end{center}

Come ultima cosa sono state prese delle misure con la resistenza $R_C$ da $\SI{2.2}{k\Omega}$ nominali, ma il segnale in ingresso risultava fortemente deformato
e asimmetrico (viene chiesta troppa corrente al generatore di funzioni), dunque aveva senso solo misurare l'ampiezza. Di seguito le misure (per via dei
pochi dati si è preferito evitare di fare un fit):
\begin{center}
    \begin{tabular}{ccc}
        \toprule
        $f [\si{kHz}]$ & $v_{in} [\si{mV}]$ & $v_{out} [\si{mV}]$ \\
        \midrule
        $9.40\pm0.01$ & $10\pm1$ & $14\pm1$ \\
        $2.34\pm0.01$ & $10\pm1$ & $53\pm1$ \\
        $0.617\pm0.001$ & $15\pm1$ & $206\pm6$ \\
        \bottomrule
    \end{tabular} 
\end{center}
Il guadagno è variato visibilmente dopo aver cambiato la resistenza, ciò fa pensare che esso non dipende
direttamente da $R_C$ quando essa varia di una piccola quantità.

