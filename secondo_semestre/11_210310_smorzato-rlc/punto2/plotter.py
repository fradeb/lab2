#laboratorio
import pylab
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

def line(x, m, q):
    return m*x+q

nomi = [20, 21, 22]
titoli = ["buone", "bruttine", "bruttozze"]

for i in range(len(nomi)):
    t, vc, vg = np.loadtxt('../dati/DSO000'+str(nomi[i])+'.CSV', delimiter=',',skiprows=2,usecols=(0,1, 2), unpack=True)

    #creo il grafico
    ls = np.linspace(0, 1000, 100)
    fig, (ax0) = plt.subplots(nrows=1, sharex=True)
    fig.suptitle("Condizioni \"" + titoli[i] + "\"")

    #plot sopra
    plot_vc = ax0.errorbar(t, vc, fmt='o', color='black', markersize='3')
    plot_vg = ax0.errorbar(t, vg, fmt='o', color='grey', markersize='3')


    ax0.legend((plot_vc, plot_vg), (r'$V_C$', r'$V_G$'), loc='upper right', shadow=True)

    ax0.set_ylabel(r'V [V]')
    ax0.set_xlabel(r't [s]')


    #salvo e mostro il grafico
    plt.savefig(str(i)+'.pdf')
    plt.show()
    plt.close()
