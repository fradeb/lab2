import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from uncertainties import unumpy as unp
from scipy.fft import fft, fftfreq
import cmath

def func(t,A, omega, tau,phi,c):
    return A*np.e**(-t/tau)*np.sin(omega*t+phi)+c


for case in range(23,26):
    t, v = np.loadtxt('../dati/DSO000'+str(case)+'.CSV', unpack=True, delimiter=',',usecols=(0,1), skiprows=2)

    t0=0
    while v[t0]<0.5*max(v):
        t0+=1

    t=t[t0:]
    v=v[t0:]
    errv = v*0.03+0.040

    res, dres = curve_fit(func, t, v, p0=[0.8,1500,0.05,0,0])
    fig, (a0, a1, a2) = plt.subplots(3, 1, gridspec_kw={'height_ratios': [4, 2, 2]})
    x=np.linspace(min(t), max(t),1000)
    t1=[]
    v1=[]
    errv1=[]
    for i in range(int(len(v)/30)):
        t1 = np.append(t1, t[30*i])
        v1 = np.append(v1, v[30*i])
        errv1 = np.append(errv1, errv[30*i])
    a0.errorbar(t1, v1, errv1, fmt='.',color='red')
    a0.plot(x,func(x,res[0],res[1],res[2],res[3],res[4]),color='black')
    a0.grid('dashed',which='both')

    a1.set_title("Residui normalizzati",size=11)
    ax=fig.add_subplot(111, frameon=False)
    ax.set_xticks([])
    ax.set_yticks([])
    a1.errorbar(t1,list(
        np.sign(v1[i]-func(t1[i],res[0],res[1],res[2],res[3],res[4]))*min(
            abs((v1[i]-func(t1[i],res[0],res[1],res[2],res[3],res[4]))/errv1[i]),
            10) for i in range(len(t1))),fmt='.',color='black')
    a1.grid('dashed',which='both')
    t, v = np.loadtxt('../dati/DSO000'+str(case+10)+'.CSV', unpack=True, delimiter=',',usecols=(0,1), skiprows=2)
    yf = fft(v)
    xf = fftfreq(len(v), t[1]-t[0])

    a2.plot(xf, np.abs(yf)/len(t))

    a2.set_xlabel('f [Hz]')
    a2.set_ylabel('Ampiezza [V]')
    a2.set_yscale('log')
    a2.set_xbound(lower=0, upper=15000)

    fig.tight_layout()
    a1.set_xlabel('Tempo [s]')
    a0.set_ylabel('ddp [V]')
    plt.grid(ls='dashed')

    for i in range(len(res)):
        print(round(res[i],3),'\pm', round(np.sqrt(dres.diagonal())[i],3), end='$&$')
    print(' ')
    print('dof:',len(t)-5,'chisq:',sum([((v1[i]-func(t1[i],res[0],res[1],res[2],res[3],res[4]))/errv1[i])**2 for i in range(len(t1))]))
    plt.savefig('./plot/'+str(case)+'.pdf')
plt.show()
