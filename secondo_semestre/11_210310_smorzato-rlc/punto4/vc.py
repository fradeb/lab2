#laboratorio
import pylab
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from scipy.fft import fft, fftfreq

def fit(t, A, B, tau, f, phi):
    return A*np.exp(-t/tau)*np.cos(2*np.pi*f*t+phi)+B

def line(x, m, q):
    return m*x+q

plt.rcParams['font.size'] = '7'

lunghi = [26]
corti = [27]

for i in range(len(lunghi)):
    #creo il grafico
    fig, (ax0, ax1, ax2) = plt.subplots(nrows=3, sharex=False)
    fig.suptitle('$V_C$')

    #plot sopra
    t, v = np.loadtxt('../dati/DSO000'+str(corti[i])+'.CSV', delimiter=',',skiprows=2,usecols=(0,1), unpack=True)

    #calcolo degli errori
    perc = 3
    delta_div = 0.002 
    #delta_div = 0.100
    
    dv = np.sqrt((perc/100*v)**2+(delta_div**2))
    ax0.errorbar(t, v, dv, fmt='o', color='black', markersize='1')

    ax0.set_xlabel('t [s]')
    ax0.set_ylabel('$V_{C}$ [V]')

    #plot sotto
    t_four, x_four = np.loadtxt('../dati/DSO000'+str(lunghi[i])+'.CSV', delimiter=',',skiprows=2,usecols=(0,1), unpack=True)
    yf = fft(x_four)
    yf = yf/len(t)
    xf = fftfreq(len(x_four), t_four[1]-t_four[0])

    ax1.plot(xf, np.abs(yf))

    ax1.set_xlabel('f [Hz]')
    ax1.set_ylabel('[u.a.]')
    ax1.set_yscale('log')
    ax1.set_xbound(lower=0, upper=1400)

    #fit 
    start = [0.6, 0, 0.02, 700, 0]
    popt, pcov = curve_fit(fit, t, v, sigma=dv, p0 = start, absolute_sigma = True)
    print(popt)
    print(np.sqrt(pcov.diagonal()))
    ls = np.linspace(0, 0.085, 1000)
    ax0.plot(ls, fit(ls, *popt), color='gray')
    ax0.set_xbound(lower=0, upper=0.025)

    #calcolo i residui normalizzati
    res = (v - fit(t, *popt))/dv

    #plot dei residui
    ax2.errorbar(t, res, fmt='o', color='black', markersize='3')
    ax2.plot(ls, line(ls, 0, 0), color='gray')


    #salvo e mostro il grafico
    plt.savefig('plot/vc.pdf')
    plt.show()
    plt.close()


