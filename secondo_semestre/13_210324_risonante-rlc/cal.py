import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from uncertainties import unumpy as unp
from scipy.fft import fft, fftfreq

am = np.array([])
bm = np.array([])
for case in range(1,4):
    t, x, y, z = np.loadtxt('dati/DSO0000'+str(case)+'.CSV', unpack=True, delimiter=',',usecols=(0,1,2,3), skiprows=2)
    plt.plot(t,x)
    plt.plot(t,y)
    plt.plot(t,z)
    plt.show()
    x = x+0.00000001
    a = (max(y)-min(y))/(max(x)-min(x))
    b = (max(z)-min(z))/(max(x)-min(x))
    am = np.append(am,a.mean())
    bm = np.append(bm,b.mean())
print(am.mean(),bm.mean())
print(am,bm)
