import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from uncertainties import unumpy as unp
from scipy.fft import fft, fftfreq

for case in range(4,9):
    t, x, y = np.loadtxt('../dati/DSO0000'+str(case)+'.CSV', unpack=True, delimiter=',',usecols=(0,1,2), skiprows=2)

    plt.figure(str(case))
    plt.xlabel("Tempo [s]")
    plt.ylabel("ddp [V]")
    plt.plot(t,x)
    plt.plot(t,y)
    plt.legend(["Input","Output"])
    plt.savefig('./plot/'+str(case)+'.pdf')
plt.show()
