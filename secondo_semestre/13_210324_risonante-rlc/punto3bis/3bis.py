#laboratorio
import pylab
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

#fft
from scipy.fft import fft, fftfreq

#random
import random

def giusto_quadrante(array):
    for i in range(len(array)):
        if(array[i]>np.pi):
            array[i]-=2*np.pi
        if(array[i]<-np.pi):
            array[i]+=2*np.pi
    return array

def sub_max(index, low, up, array):
    max_index = max(index-low, 0)
    for i in range(max(index-low, 0), min(index+up, len(array))):
        if(array[i] > array[max_index]):
            max_index = i

    massimi = []
    for i in range(max(index-low, 0), min(index+up, len(array))):
        if(array[i] == array[max_index]):
            massimi.append(i)

    return massimi[random.randrange(len(massimi))]

def semi_dispersione(array):
    return abs(array-array.mean()).max()

def fit_func(x, A, freq, phi, B):
    return np.abs(A)*np.sin(2*np.pi*freq*t+phi)+B

def line(x, m, q):
    return m*x+q

def fit(t, ch, case, ch_no):
    #creo il grafico
    fig, (ax0, ax1) = plt.subplots(nrows=2, sharex=False)

    #plotto i dati
    ax0.errorbar(t, ch, fmt='o', color='black', markersize='1')

    #calcolo la fft
    yf = fft(ch)
    xf = fftfreq(len(t), t[1]-t[0])

    #plotto la fft
    ax1.plot(xf, np.abs(yf))
    ax1.set_xlabel('f [Hz]')
    ax1.set_yscale('log')

    #la fft mi serve per trovare la frequenza di oscillazione. Trovo l'indice dell'elemento massimo nell'array abs(yf).
    freq = abs(xf[np.argmax(abs(yf))])
    print(freq)

    #la frequenza mi serve per azzeccare il fit giusto
    #gestione dei casi particolari
    P0 = [1, freq, 0, 0]
    if(case == 19 and ch_no == 2):
        P0 = [1, 1571, 0, 0]
    if(case == 19 and ch_no == 3):
        P0 = [1, 1571, 0, 0]
    if(case == 25 and ch_no == 3):
        P0 = [1, 2360, 0, 0]
    if(case == 21 and ch_no == 2):
        P0 = [1, 1751, 0, 0]
    if(case == 21 and ch_no == 3):
        P0 = [1, 1751, 0, 0]
    if(case == 23 and ch_no == 3):
        P0 = [1, 2050, 0, 0]
    if(case == 24 and ch_no == 2):
        P0 = [1, 2150, 0, 0]
    if(case == 25 and ch_no == 2):
        P0 = [1, 2360, 0, 0]
    if(case == 26 and ch_no == 2):
        P0 = [1, 2545, 0, 0]
    if(case == 27 and ch_no == 2):
        P0 = [1, 2810, 0, 0]
    if(case == 28 and ch_no == 2):
        P0 = [1, 3105, 0, 0]
    if(case == 29 and ch_no == 2):
        P0 = [1, 3659.50, -1.6137, 0]
    if(case == 30 and ch_no == 2):
        P0 = [1, 5242.23975, -1.64, 0]
    if(case == 31 and ch_no == 2):
        P0 = [1, 7555, -1.6122, 0]
    if(case == 31 and ch_no == 3):
        P0 = [1, 7555, -1.6122, 0]
    if(case == 32 and ch_no == 2):
        P0 = [1, 9987.21568, -1.63449, 0]

    #calcolo i parametri del fit
    popt, pcov = curve_fit(fit_func, t, ch, p0=P0)

    #plotto il risultato del fit
    print(popt)
    ax0.plot(t, fit_func(t, *popt), color='gray')

    #salvo e mostro il grafico
    #plt.show()
    plt.close()

    return popt

def detezione(t, ch, popt):
    freq = popt[1]
    phi = popt[2]
    low = 50
    up = 90

    #massimi
    value = np.pi/2
    m_min = np.ceil((2*np.pi*freq*np.amin(t)+phi-value)/(2*np.pi)).astype('int')
    m_max = np.ceil((2*np.pi*freq*np.amax(t)+phi-value)/(2*np.pi)).astype('int')
    t_massimi = []
    ch_massimi = []
    for m in range(m_min, m_max):
        index=np.abs(t-(value+2*np.pi*m-phi)/(2*np.pi*freq)).argmin()
        index = sub_max(index, low, up, ch)
        t_massimi.append(t[index])
        ch_massimi.append(ch[index])

    #minimi
    value = -np.pi/2
    m_min = np.ceil((2*np.pi*freq*np.amin(t)+phi-value)/(2*np.pi)).astype('int')
    m_max = np.ceil((2*np.pi*freq*np.amax(t)+phi-value)/(2*np.pi)).astype('int')
    t_minimi = []
    ch_minimi = []
    for m in range(m_min, m_max):
        index=np.abs(t-(value+2*np.pi*m-phi)/(2*np.pi*freq)).argmin()
        index = sub_max(index, low, up, -ch)
        t_minimi.append(t[index])
        ch_minimi.append(ch[index])

    #creo il grafico
    fig, (ax0) = plt.subplots(nrows=1, sharex=False)
    ax0.errorbar(t, ch, fmt='o', color='black', markersize='1')
    ax0.errorbar(t_massimi, ch_massimi, fmt='o', color='blue', markersize='4')
    ax0.errorbar(t_minimi, ch_minimi, fmt='o', color='orange', markersize='4')
    #plt.show()
    plt.close()

    return freq, picco(ch_minimi, ch_massimi), t_minimi, t_massimi

#si intende che chm contiene i minimi, chM contiene i massimi
def picco(chm, chM):
    l = min(len(chm), len(chM))
    chm = np.array(chm[0:l])
    chM = np.array(chM[0:l])

    v = (chM-chm)/2
    return v.mean(), semi_dispersione(v)

def sfasamento(freq, t1m, t1M, t2m, t2M):
    lm = min(len(t1m), len(t2m))
    t1m = np.array(t1m[0:lm])
    t2m = np.array(t2m[0:lm])
    lM = min(len(t1M), len(t2M))
    t1M = np.array(t1M[0:lM])
    t2M = np.array(t2M[0:lM])

    t1 = np.append(t1m, t1M)
    t2 = np.append(t2m, t2M)

    phi = giusto_quadrante((t2-t1)*freq*2*np.pi)
    return phi.mean(), semi_dispersione(phi)




start = 9
end = 33

f = []

#quadagno
A = []
dA = []

#sfasamento
phi = []
dphi = []

#guadagno C
AC = []
dAC = []

#sfasamento C
phiC = []
dtanphiC = []
dphiC = []

for case in range(start, end):
    print(case)
    t, ch1, ch2, ch3 = np.loadtxt('../dati/DSO000'+str(case).zfill(2)+'.CSV', unpack=True, delimiter=',',usecols=(0,1, 2, 3), skiprows=2)
    freq, (V1, dV1), t1m, t1M = detezione(t, ch1, fit(t, ch1, case, 1))
    freq, (V2, dV2), t2m, t2M = detezione(t, ch2, fit(t, ch2, case, 2))
    freq, (V3, dV3), t3m, t3M = detezione(t, ch1-ch3, fit(t, ch1-ch3, case, 3))

    f.append(freq)

    #guadagno
    A.append(V2/V1)
    dA.append(np.sqrt((dV2/V2)**2+(dV1/V1)**2))

    #sfasamento
    phi.append(sfasamento(freq, t1m, t1M, t2m, t2M)[0])
    dphi.append(sfasamento(freq, t1m, t1M, t2m, t2M)[1])

    #guadagno C
    AC.append(V3/V1)
    dAC.append(max(np.sqrt((dV3/V3)**2+(dV1/V1)**2),0.01*AC[-1]))

    #sfasamento C
    phiC.append(sfasamento(freq, t1m, t1M, t3m, t3M)[0])
    dphiC.append(sfasamento(freq, t1m, t1M, t3m, t3M)[1])
    dtanphiC.append(min(sfasamento(freq, t1m, t1M, t3m, t3M)[1]*(1+np.tan(phiC[-1])**2),10))
dAC[len(f)-1]=dAC[len(f)-1]*3

for i in range(len(A)):
    print('$',round(f[i]),'$&$',round(A[i],4),'\pm',round(dA[i],4),'$&$',round(phi[i],4),'\pm',round(dphi[i],4),'$&$',round(AC[i],4),'\pm',round(dAC[i],4),'$&$',round(phiC[i],4),'\pm',round(dphiC[i],4),'$\\\\')

def guad(f, R,r,L,C):
    return R/np.sqrt((R+r)**2+(2*np.pi*f*L-1/(2*np.pi*f*C))**2)

def guadC(f,R,L,C):
    return np.sqrt(1/((1-4*np.pi**2*f**2*L*C)**2+(R*C*2*np.pi*f)**2))

def sfas(f, R,L,C):
    return np.arctan((2*np.pi*f*L-1/(2*np.pi*f*C))/(R))

def sfasC(f,R,L,C):
    return -R/(2*np.pi*f*L-1/(2*np.pi*f*C))

#Uso le tangenti per comodità
phiC = np.tan(phiC)
dphiC = dtanphiC

x = np.linspace(min(f),max(f),700)
res, pcov = curve_fit(guad, f, A, sigma=dA, p0=[70,15,0.1,0.1*10**-6])
fig, (a0, a1) = plt.subplots(2, 1, gridspec_kw={'height_ratios': [4, 2]})
a0.errorbar(f, A, dA, fmt='.',color='red')
a0.plot(x,guad(x,res[0],res[1],res[2],res[3]),color='black')
a0.grid('dashed',which='both')

a1.set_title("Residui normalizzati",size=11)
ax=fig.add_subplot(111, frameon=False)
ax.set_xticks([])
ax.set_yticks([])
a1.errorbar(f,list((guad(f[i],res[0],res[1],res[2],res[3])-A[i])/dA[i] for i in range(len(f))),fmt='.',color='black')
a1.grid('dashed',which='both')
fig.tight_layout()
a1.set_xlabel('Frequenza [Hz]')
a0.set_ylabel('Guadagno ')
plt.grid(ls='dashed')
#plt.show()
#plt.savefig('./plot/A.pdf')
plt.close()

chisq = sum(list((guad(f[i],res[0],res[1],res[2],res[3])-A[i])**2/dA[i]**2 for i in range(len(f))))
dres = np.sqrt(pcov.diagonal())
print('$',res[0],'\pm',dres[0],'$&$',res[1],'\pm',dres[1],'$&$',res[2],'\pm',dres[2],'$&$',res[3],'\pm',dres[3],'$&$',chisq,'$&$',len(f)-4,'$\\\\')

res, pcov = curve_fit(sfas, f, phi, sigma=dphi, p0=[85,0.1,0.1*10**-6])
fig, (a0, a1) = plt.subplots(2, 1, gridspec_kw={'height_ratios': [4, 2]})
a0.errorbar(f, phi, dphi, fmt='.',color='red')
a0.plot(x,sfas(x,res[0],res[1],res[2]),color='black')
a0.grid('dashed',which='both')

a1.set_title("Residui normalizzati",size=11)
ax=fig.add_subplot(111, frameon=False)
ax.set_xticks([])
ax.set_yticks([])
a1.errorbar(f,list((sfas(f[i],res[0],res[1],res[2])-phi[i])/dphi[i] for i in range(len(f))),fmt='.',color='black')
a1.grid('dashed',which='both')
fig.tight_layout()
a1.set_xlabel('Frequenza [Hz]')
a0.set_ylabel('$\Delta \phi$ [rad]')
plt.grid(ls='dashed')

#plt.show()
#plt.savefig('./plot/phi.pdf')
plt.close()

chisq = sum(list((sfas(f[i],res[0],res[1],res[2])-phi[i])**2/dphi[i]**2 for i in range(len(f))))
dres = np.sqrt(pcov.diagonal())
print('$',res[0],'\pm',dres[0],'$&$',res[1],'\pm',dres[1],'$&$',res[2],'\pm',dres[2],'$&$',chisq,'$&$',len(f)-3,'$\\\\')

res, pcov = curve_fit(guadC, f, AC, sigma=dAC, p0=[85,0.1,0.1*10**-6])
fig, (a0, a1) = plt.subplots(2, 1, gridspec_kw={'height_ratios': [4, 2]})
a0.errorbar(f, AC, dAC, fmt='.',color='red')
a0.plot(x,guadC(x,res[0],res[1],res[2]),color='black')
a0.grid('dashed',which='both')

a1.set_title("Residui normalizzati",size=11)
ax=fig.add_subplot(111, frameon=False)
ax.set_xticks([])
ax.set_yticks([])
a1.errorbar(f,list((guadC(f[i],res[0],res[1],res[2])-AC[i])/dAC[i] for i in range(len(f))),fmt='.',color='black')
a1.grid('dashed',which='both')
fig.tight_layout()
a1.set_xlabel('Frequenza [Hz]')
a0.set_ylabel('Guadagno')
plt.grid(ls='dashed')
plt.show()
#plt.savefig('./plot/AC.pdf')
plt.close()

chisq = sum(list((guadC(f[i],res[0],res[1],res[2])-AC[i])**2/dAC[i]**2 for i in range(len(f))))
dres = np.sqrt(pcov.diagonal())
print('$',res[0],'\pm',dres[0],'$&$',res[1],'\pm',dres[1],'$&$',res[2],'\pm',dres[2],'$&$',chisq,'$&$',len(f)-4,'$\\\\')

res, pcov = curve_fit(sfasC, f, phiC, sigma=dphiC, p0=[96,0.1,0.1*10**-6])
#print(res)
fig, (a0, a1) = plt.subplots(2, 1, gridspec_kw={'height_ratios': [4, 2]})
a0.errorbar(f, phiC, dphiC, fmt='.',color='red')
#a0.plot(x,sfasC(x,res[0],res[1],res[2]),color='black')
a0.plot(x,sfasC(x,83,0.1,0.1*10**-6),color='black')
a0.grid('dashed',which='both')

a1.set_title("Residui normalizzati",size=11)
ax=fig.add_subplot(111, frameon=False)
ax.set_xticks([])
ax.set_yticks([])
a1.errorbar(f,list((sfasC(f[i],res[0],res[1],res[2])-phiC[i])/dphiC[i] for i in range(len(f))),fmt='.',color='black')
a1.grid('dashed',which='both')
fig.tight_layout()
a1.set_xlabel('Frequenza [Hz]')
a0.set_ylabel('tan($\Delta \phi$)', labelpad=-5)
plt.grid(ls='dashed')
#plt.show()
#plt.savefig('./plot/phiC.pdf')
plt.close()

chisq = sum(list((sfasC(f[i],res[0],res[1],res[2])-phiC[i])**2/dphiC[i]**2 for i in range(len(f))))
dres = np.sqrt(pcov.diagonal())
print('$',res[0],'\pm',dres[0],'$&$',res[1],'\pm',dres[1],'$&$',res[2],'\pm',dres[2],'$&$',chisq,'$&$',len(f)-3,'$\\\\')
