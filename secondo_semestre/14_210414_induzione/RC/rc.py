import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from uncertainties import unumpy as unp
from scipy.fft import fft, fftfreq

t, x, y = np.loadtxt('../dati/DSO00066.CSV', unpack=True, delimiter=',',usecols=(0,1,2), skiprows=85)

def expo(x, tau, A, offset):
    return A*np.exp(-x/tau)+offset

def media(x):
    n=30
    y=np.zeros(int(len(x)/n))
    for i in range(len(y)):
        y[i] = np.mean(x[n*i:n*i+n])
    return y

tagli = [4415,4450,8800]
x1 = x[:tagli[0]]
y1 = y[:tagli[0]]
erry = [0.4]*len(y1)
t1 = t[:tagli[0]]
t1 = t1 - t1[0]
xm = np.mean(x1)
ris, dris = curve_fit(expo, t1, y1,sigma=erry, p0=[0.0007,y1[0]-xm,xm], absolute_sigma=True)

k = np.linspace(min(t1),max(t1),1000)
fig, (a0, a1) = plt.subplots(2, 1, gridspec_kw={'height_ratios': [4, 2]})
a0.errorbar(media(t1), media(y1),media(erry), fmt='.',color='red')
a0.plot(k,expo(k,ris[0],ris[1],ris[2]),color='black')
a0.grid('dashed',which='both')

a1.set_title("Residui normalizzati",size=11)
ax = fig.add_subplot(111, frameon=False)
ax.set_xticks([])
ax.set_yticks([])
a1.errorbar(media(t1),list((expo(media(t1)[i],ris[0],ris[1],ris[2])-media(y1)[i])/media(erry)[i] for i in range(len(media(y1)))),fmt='.',color='black')
a1.grid('dashed',which='both')
fig.tight_layout()
a1.set_xlabel('Tempo [s]')
a0.set_ylabel('ddp [V]')
plt.grid(ls='dashed')
plt.savefig('./plot/1.pdf')
plt.show()

errris = np.sqrt(dris.diagonal())
dof = len(t1-3)
chisq = sum([(expo(t1[i],ris[0],ris[1],ris[2])-y1[i])**2/erry[i]**2 for i in range(len(y1))])
print('$',ris[0],'\pm',errris[0],'$&$',ris[1],'\pm',errris[1],'$&$',ris[2],'\pm',errris[2],'$&$',dof,'\pm',chisq)
print(dris)

x2 = x[tagli[1]:tagli[2]]
y2 = y[tagli[1]:tagli[2]]
erry = [0.4]*len(y2)
t2 = t[tagli[1]:tagli[2]]
t2 = t2 - t2[0]
xm = np.mean(x2)
ris, dris = curve_fit(expo, t2, y2, sigma=erry,p0=[0.0007,y2[0]-xm,xm], absolute_sigma=True)

k = np.linspace(min(t2),max(t2),1000)
fig, (a0, a1) = plt.subplots(2, 1, gridspec_kw={'height_ratios': [4, 2]})
a0.errorbar(media(t2), media(y2), media(erry),fmt='.',color='red')
a0.plot(k,expo(k,ris[0],ris[1],ris[2]),color='black')
a0.grid('dashed',which='both')

a1.set_title("Residui normalizzati",size=11)
ax = fig.add_subplot(111, frameon=False)
ax.set_xticks([])
ax.set_yticks([])
a1.errorbar(media(t2),list((expo(media(t2)[i],ris[0],ris[1],ris[2])-media(y2)[i])/media(erry)[i] for i in range(len(media(y2)))),fmt='.',color='black')
a1.grid('dashed',which='both')
fig.tight_layout()
a1.set_xlabel('Tempo [s]')
a0.set_ylabel('ddp [V]')
plt.grid(ls='dashed')
plt.savefig('./plot/2.pdf')
plt.show()

errris = np.sqrt(dris.diagonal())
dof = len(t2-3)
chisq = sum([(expo(t2[i],ris[0],ris[1],ris[2])-y2[i])**2/erry[i]**2 for i in range(len(y2))])
print('$',ris[0],'\pm',errris[0],'$&$',ris[1],'\pm',errris[1],'$&$',ris[2],'\pm',errris[2],'$&$',dof,'\pm',chisq)
print(dris)

