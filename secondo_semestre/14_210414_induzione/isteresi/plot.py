#laboratorio
import pylab
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

t, VH, VB = np.loadtxt('../dati/DSO00069.CSV', delimiter=',',skiprows=2,usecols=(0,1, 2), unpack=True)
dVB = VB*0.03+0.002
dVH = VH*0.03+0.1
plt.errorbar(VH, VB, dVB, dVH)
plt.xlabel(r'$V_H$ [V]')
plt.ylabel(r'$V_B$ [V]')
plt.grid(color='gray', linestyle='-', linewidth=1)
plt.title(r'Trasformatore $24 V_{rms}$')
plt.savefig('24.pdf')
#plt.show()
plt.close()

t, VH, VB = np.loadtxt('../dati/DSO00070.CSV', delimiter=',',skiprows=2,usecols=(0,1, 2), unpack=True)
dVB = VB*0.03+0.002
dVH = VH*0.03+0.1
plt.errorbar(VH, VB, dVB, dVH)
plt.xlabel(r'$V_H$ [V]')
plt.ylabel(r'$V_B$ [V]')
plt.grid(color='gray', linestyle='-', linewidth=1)
plt.title(r'Trasformatore $48 V_{rms}$')
plt.savefig('48.pdf')
#plt.show()
plt.close()
