import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from uncertainties import unumpy as unp
from scipy.fft import fft, fftfreq

t1, x = np.loadtxt('../dati/DSO00067.CSV', unpack=True, delimiter=',',usecols=(0,1), skiprows=80)
t2, y = np.loadtxt('../dati/DSO00068.CSV', unpack=True, delimiter=',',usecols=(0,1), skiprows=80)
errx = [0.4]*len(x)
erry = [0.4]*len(y)

def quadra(x, A, omega, phi, offset):
    return A*np.sign(np.cos(omega*x+phi))+offset

def media(x):
    n=30
    y=np.zeros(int(len(x)/n))
    for i in range(len(y)):
        y[i] = np.mean(x[n*i:n*i+n])
    return y

ris1, dris1 = curve_fit(quadra, t1, x, sigma=[0.4]*len(t1),p0=[-8,710,-1.56,0.5])
ris2, dris2 = curve_fit(quadra, t2, y, sigma=[0.4]*len(t2),p0=[-4,710,-1.56,0.5])

k = np.linspace(min(t1),max(t1),1000)
fig, (a0) = plt.subplots(1, 1, gridspec_kw={'height_ratios': [1]})
a0.errorbar(media(t1), media(x),media(errx), fmt='.',color='red')
a0.plot(k,quadra(k,ris1[0],ris1[1],ris1[2],ris1[3]),color='black')
a0.grid('dashed',which='both')
plt.savefig('./plot/1.pdf')

errris1 = np.sqrt(dris1.diagonal())
dof = len(t1-3)
chisq = sum([(quadra(t1[i],ris1[0],ris1[1],ris1[2],ris1[3])-x[i])**2/errx[i]**2 for i in range(len(x))])
print('$',ris1[0],'\pm',errris1[0],'$&$',ris1[1],'\pm',errris1[1],'$&$',ris1[2],'\pm',errris1[2],'$&$',dof,'\pm',chisq)

fig, (a0) = plt.subplots(1, 1, gridspec_kw={'height_ratios': [1]})
a0.errorbar(media(t1), media(y),media(erry), fmt='.',color='red')
a0.plot(k,quadra(k,ris2[0],ris2[1],ris2[2],ris2[3]),color='black')
a0.grid('dashed',which='both')
plt.savefig('./plot/2.pdf')

errris2 = np.sqrt(dris2.diagonal())
dof = len(t1-3)
chisq = sum([(quadra(t1[i],ris2[0],ris2[1],ris2[2],ris2[3])-y[i])**2/erry[i]**2 for i in range(len(y))])
print('$',ris2[0],'\pm',errris2[0],'$&$',ris2[1],'\pm',errris2[1],'$&$',ris2[2],'\pm',errris2[2],'$&$',dof,'\pm',chisq)

